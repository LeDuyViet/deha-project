<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreateProductsTest extends TestCase
{
    /**
     * @test
     */
    public function authenticated_super_admin_can_new_create_product()
    {
        $this->withoutExceptionHandling();
        $this->loginWithSuperAdmin();
        $product = $this->makeFactoryProduct();
        $response = $this->post($this->getStoreProductRoute(), $product);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('data', fn(AssertableJson $json) =>
                $json->where('name', $product['name'])->etc()
            )->etc()
        );
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_see_text_error_if_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $product = $this->makeFactoryProduct([
            'name' => null
        ]);
        $response = $this->post($this->getStoreProductRoute(), $product);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors', fn(AssertableJson $json) =>
        $json->has('name')
        )
        );
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_see_text_error_if_price_is_null()
    {
        $this->loginWithSuperAdmin();
        $product = $this->makeFactoryProduct([
            'price' => null,
        ]);
        $response = $this->post($this->getStoreProductRoute(), $product);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors', fn(AssertableJson $json) =>
        $json->has('price')
        )
        );
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_see_text_error_if_description_is_null()
    {
        $this->loginWithSuperAdmin();
        $product = $this->makeFactoryProduct([
            'description' => null,
        ]);
        $response = $this->post($this->getStoreProductRoute(), $product);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors', fn(AssertableJson $json) =>
        $json->has('description')
        )
        );
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_see_text_error_if_data_is_null()
    {
//        $this->withoutExceptionHandling();
        $this->loginWithSuperAdmin();
        $product = $this->makeFactoryProduct([
            'name' => null,
            'price' => null,
            'description' => null
        ]);
        $response = $this->post($this->getStoreProductRoute(), $product);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors', fn(AssertableJson $json) =>
        $json->has('name')
             ->has('description')
             ->has('price')
        )
        );
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_new_create_product()
    {
        $this->withoutExceptionHandling();
        $this->loginUserWithPermissions('product-store');
        $product = $this->makeFactoryProduct();
        $response = $this->post($this->getStoreProductRoute(), $product);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('data', fn(AssertableJson $json) =>
                $json->where('name', $product['name'])->etc()
            )->etc()
        );
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_see_text_error_if_name_is_null()
    {
        $this->loginUserWithPermissions('product-store');
        $product = $this->makeFactoryProduct([
            'name' => null
        ]);
        $response = $this->post($this->getStoreProductRoute(), $product);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors', fn(AssertableJson $json) =>
        $json->has('name')
        )
        );
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_see_text_error_if_price_is_null()
    {
        $this->loginUserWithPermissions('product-store');
        $product = $this->makeFactoryProduct([
            'price' => null,
        ]);
        $response = $this->post($this->getStoreProductRoute(), $product);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors', fn(AssertableJson $json) =>
        $json->has('price')
        )
        );
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_see_text_error_if_description_is_null()
    {
        $this->loginUserWithPermissions('product-store');
        $product = $this->makeFactoryProduct([
            'description' => null,
        ]);
        $response = $this->post($this->getStoreProductRoute(), $product);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors', fn(AssertableJson $json) =>
        $json->has('description')
        )
        );
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_see_text_error_if_data_is_null()
    {
//        $this->withoutExceptionHandling();
        $this->loginUserWithPermissions('product-store');
        $product = $this->makeFactoryProduct([
            'name' => null,
            'price' => null,
            'description' => null
        ]);
        $response = $this->post($this->getStoreProductRoute(), $product);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors', fn(AssertableJson $json) =>
        $json->has('name')
             ->has('description')
             ->has('price')
        )
        );
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_not_create_new_product()
    {
        $product = $this->makeFactoryProduct();
        $response = $this->post($this->getStoreProductRoute(), $product);
        $response->assertStatus(Response::HTTP_FOUND);
    }

    public function getListProductRoute()
    {
        return route('products.index');
    }

    public function getStoreProductRoute()
    {
        return route('products.store');
    }

    public function makeFactoryProduct(array $customFile = [])
    {
        return Product::factory()->make($customFile)->toArray();
    }
}
