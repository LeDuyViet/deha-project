<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListProductsTest extends TestCase
{
    /**
     * @test
     */
    public function authenticated_super_admin_user_can_get_list_products()
    {
        $this->loginWithSuperAdmin();
        $product = Product::factory()->create();
        $response = $this->get(route('products.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_not_get_list_products()
    {
        $response = $this->get(route('products.index'));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_get_list_products()
    {
        $this->loginUserWithPermissions('product-view');
        $product = Product::factory()->create();
        $response = $this->get(route('products.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
        $response->assertSee($product->display_name);

    }
}
