<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class GetShowProductsTest extends TestCase
{
    /**
     * @test
     */
    public function authenticated_super_admin_can_get_product()
    {
        $this->loginWithSuperAdmin();
        $product = $this->createFactoryProduct();
        $response = $this->get($this->getShowProductRoute($product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('data', fn(AssertableJson $json) =>
                $json->where('name', $product->name)
                     ->etc()
            )->etc()
        );
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_not_get_product_if_product_not_exists()
    {
        $this->loginWithSuperAdmin();
        $productId = -1;
        $response = $this->get($this->getShowProductRoute($productId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('message')
        );
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_get_product()
    {
        $this->loginUserWithPermissions('product-show');
        $product = $this->createFactoryProduct();
        $response = $this->get($this->getShowProductRoute($product->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('data', fn(AssertableJson $json) =>
        $json->where('name', $product->name)
            ->etc()
        )->etc()
        );
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_not_get_product_if_product_not_exists()
    {
        $this->loginUserWithPermissions('product-show');
        $productId = -1;
        $response = $this->get($this->getShowProductRoute($productId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('message')
        );
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_not_show_product()
    {
        $product = $this->createFactoryProduct();
        $response = $this->get($this->getShowProductRoute($product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    public function createFactoryProduct(array $customFiled = [])
    {
        return Product::factory()->create($customFiled);
    }

    public function getShowProductRoute($id)
    {
        return route('products.show', $id);
    }
}
