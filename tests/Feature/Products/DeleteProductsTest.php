<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeleteProductsTest extends TestCase
{
    //    Super Admin
    /**
     * @test
     */
    public function authenticated_super_admin_can_delete_product_if_product_exists()
    {
        $this->loginWithSuperAdmin();
        $product = $this->createFactoryProduct();
        $productCountBefore = Product::count();
        $response = $this->delete($this->getDeleteProductRoute($product->id));
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('message')
            ->etc()
        );
        $this->assertDatabaseCount('products', $productCountBefore - 1);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_not_delete_product_if_product_not_exists()
    {
        $this->loginWithSuperAdmin();
        $productId = -1;
        $response = $this->delete($this->getDeleteProductRoute($productId));
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('message')
            ->etc()
        );
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    //    User Have Permission
    /**
     * @test
     */
    public function authenticated_user_have_permission_can_delete_product_if_product_exists()
    {
        $this->loginUserWithPermissions('product-delete');
        $product = $this->createFactoryProduct();
        $productCountBefore = Product::count();
        $response = $this->delete($this->getDeleteProductRoute($product->id));
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('message')
            ->etc()
        );
        $this->assertDatabaseCount('products', $productCountBefore - 1);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_not_delete_product_if_product_not_exists()
    {
        $this->loginUserWithPermissions('product-delete');
        $productId = -1;
        $response = $this->delete($this->getDeleteProductRoute($productId));
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('message')
            ->etc()
        );
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_not_delete_product()
    {
        $productId = 1;
        $response = $this->delete($this->getDeleteProductRoute($productId));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    public function createFactoryProduct(array $customFiled = [])
    {
        return Product::factory()->create($customFiled);
    }

    public function getDeleteProductRoute($id): string
    {
        return route('products.delete', $id);
    }
}
