<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdateProductsTest extends TestCase
{
//    Super Admin
    /**
     * @test
     */
    public function authenticated_super_admin_can_update_product()
    {
//        $this->withoutExceptionHandling();
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryProduct();
        $product = $this->createFactoryProduct();
        $response = $this->put($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('data', fn(AssertableJson $json) =>
                $json->where('name', $dataUpdate['name'])
                     ->where('price', $dataUpdate['price'])
                     ->where('description', $dataUpdate['description'])
                     ->etc()
            )->etc()
        );
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_can_not_update_product_if_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryProduct([
            'name' => null,
        ]);
        $product = $this->createFactoryProduct();
        $response = $this->put($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors', fn(AssertableJson $json) =>
        $json->has('name')
        )
        );
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_can_not_update_product_if_price_is_null()
    {
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryProduct([
            'price' => null,
        ]);
        $product = $this->createFactoryProduct();
        $response = $this->put($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors', fn(AssertableJson $json) =>
        $json->has('price')
        )
        );
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_can_not_update_product_if_description_is_null()
    {
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryProduct([
            'description' => null,
        ]);
        $product = $this->createFactoryProduct();
        $response = $this->put($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors', fn(AssertableJson $json) =>
        $json->has('description')
        )
        );
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_can_not_update_product_if_data_is_null()
    {
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryProduct([
            'name' => null,
            'price' => null,
            'description' => null,
        ]);
        $product = $this->createFactoryProduct();
        $response = $this->put($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors', fn(AssertableJson $json) =>
        $json->has('name')
             ->has('price')
             ->has('description')
        )
        );
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_can_not_update_product_if_product_is_invalid()
    {
        $this->loginWithSuperAdmin();
        $productId = -1;
        $dataUpdate = $this->makeFactoryProduct();
        $product = $this->createFactoryProduct();
        $response = $this->put($this->getUpdateProductRoute($productId), $dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->where('message', 'Record not found')
        );
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_can_not_update_product_if_image_field_is_not_image()
    {
        $this->loginWithSuperAdmin();
        $file = UploadedFile::fake()->create(
            'document.pdf', 123, 'application/pdf'
        );
        $dataUpdate = $this->makeFactoryProduct([
            'image' => $file,
        ]);
        $product = $this->createFactoryProduct();
        $response = $this->putJson($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors', fn(AssertableJson $json) =>
            $json->has('image')
        )
        );
    }

//    User have permission
    /**
     * @test
     */
    public function authenticated_user_have_permission_can_update_product()
    {
//        $this->withoutExceptionHandling();
        $this->loginUserWithPermissions('product-update');
        $dataUpdate = $this->makeFactoryProduct();
        $product = $this->createFactoryProduct();
        $response = $this->put($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) =>
            $json->has('data', fn(AssertableJson $json) =>
                $json->where('name', $dataUpdate['name'])
                     ->where('price', $dataUpdate['price'])
                     ->where('description', $dataUpdate['description'])
                     ->etc()
            )->etc()
        );
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_can_not_update_product_if_name_is_null()
    {
        $this->loginUserWithPermissions('product-update');
        $dataUpdate = $this->makeFactoryProduct([
            'name' => null,
        ]);
        $product = $this->createFactoryProduct();
        $response = $this->put($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors', fn(AssertableJson $json) =>
        $json->has('name')
        )
        );
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_can_not_update_product_if_price_is_null()
    {
        $this->loginUserWithPermissions('product-update');
        $dataUpdate = $this->makeFactoryProduct([
            'price' => null,
        ]);
        $product = $this->createFactoryProduct();
        $response = $this->put($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors', fn(AssertableJson $json) =>
        $json->has('price')
        )
        );
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_can_not_update_product_if_description_is_null()
    {
        $this->loginUserWithPermissions('product-update');
        $dataUpdate = $this->makeFactoryProduct([
            'description' => null,
        ]);
        $product = $this->createFactoryProduct();
        $response = $this->put($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors', fn(AssertableJson $json) =>
        $json->has('description')
        )
        );
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_can_not_update_product_if_data_is_null()
    {
        $this->loginUserWithPermissions('product-update');
        $dataUpdate = $this->makeFactoryProduct([
            'name' => null,
            'price' => null,
            'description' => null,
        ]);
        $product = $this->createFactoryProduct();
        $response = $this->put($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors', fn(AssertableJson $json) =>
        $json->has('name')
             ->has('price')
             ->has('description')
        )
        );
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_can_not_update_product_if_product_is_invalid()
    {
        $this->loginUserWithPermissions('product-update');
        $productId = -1;
        $dataUpdate = $this->makeFactoryProduct();
        $product = $this->createFactoryProduct();
        $response = $this->put($this->getUpdateProductRoute($productId), $dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->where('message', 'Record not found')
        );
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_can_not_update_product_if_image_field_is_not_image()
    {
        $this->loginUserWithPermissions('product-update');
        $file = UploadedFile::fake()->create(
            'document.pdf', 123, 'application/pdf'
        );
        $dataUpdate = $this->makeFactoryProduct([
            'image' => $file,
        ]);
        $product = $this->createFactoryProduct();
        $response = $this->putJson($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) =>
        $json->has('errors', fn(AssertableJson $json) =>
        $json->has('image')
        )
        );
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_not_update_products()
    {
        $product = $this->createFactoryProduct();
        $dataUpdate = $this->makeFactoryProduct();
        $response = $this->put($this->getUpdateProductRoute($product->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }


    public function getListProductRoute()
    {
        return route('products.index');
    }

    public function getUpdateProductRoute($id)
    {
        return route('products.update', $id);
    }

    public function makeFactoryProduct(array $customFiled = [])
    {
        return Product::factory()->make($customFiled)->toArray();
    }

    public function createFactoryProduct(array $customFiled = [])
    {
        return Product::factory()->create($customFiled);
    }

}
