<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetShowUserTest extends TestCase
{
    /**
     * @test
     */
    public function authenticated_super_admin_can_get_user()
    {
        $this->loginWithSuperAdmin();
        $user = $this->createFactoryUser();
        $response = $this->get($this->getShowUserRoute($user->id));
        $response->assertViewIs('users.show');
        $response->assertSee($user->name);
        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_not_get_user_if_user_not_exists()
    {
        $this->loginWithSuperAdmin();
        $userId = -1;
        $response = $this->get($this->getShowUserRoute($userId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_get_user()
    {
        $this->loginUserWithPermissions('user-show');
        $user = $this->createFactoryUser();
        $response = $this->get($this->getShowUserRoute($user->id));
        $response->assertViewIs('users.show');
        $response->assertSee($user->name);
        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_not_get_user_if_user_not_exists()
    {
        $this->loginUserWithPermissions('user-show');
        $userId = -1;
        $response = $this->get($this->getShowUserRoute($userId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_not_show_user()
    {
        $user = $this->createFactoryUser();
        $response = $this->get($this->getShowUserRoute($user->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    public function createFactoryUser(array $customFiled = [])
    {
        return User::factory()->create($customFiled);
    }

    public function getShowUserRoute($id)
    {
        return route('users.show', $id);
    }
}
