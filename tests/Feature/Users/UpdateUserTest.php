<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateUserTest extends TestCase
{
//    Super Admin
    /**
     * @test
     */
    public function authenticated_super_admin_can_see_update_user_form()
    {
//        $this->withoutExceptionHandling();
        $this->loginWithSuperAdmin();
        $user = $this->createFactoryUser();
        $response = $this->get($this->getEditUserRoute($user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.edit');
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_update_user()
    {
        $this->withoutExceptionHandling();
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryUser();
        $user = $this->createFactoryUser();
        $response = $this->put($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getListUserRoute());
        $this->assertDatabaseHas('users', $dataUpdate);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_see_text_error_if_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $user = $this->createFactoryUser();
        $dataUpdate = $this->makeFactoryUser([
            'name' => null
        ]);
        $user = $this->createFactoryUser();
        $response = $this->from($this->getListUserRoute($user->id))->put($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_see_text_error_if_email_is_null()
    {
        $this->loginWithSuperAdmin();
        $user = $this->createFactoryUser();
        $dataUpdate = $this->makeFactoryUser([
            'email' => null
        ]);
        $user = $this->createFactoryUser();
        $response = $this->from($this->getListUserRoute($user->id))->put($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['email']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_see_text_error_if_data_is_null()
    {
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryUser([
            'name' => null,
            'email' => null,
            'phone_number' => null,
        ]);
        $user = $this->createFactoryUser();
        $response = $this->from($this->getListUserRoute($user->id))->put($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name', 'email', 'phone_number']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_can_not_update_user_if_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryUser([
            'name' => null,
        ]);
        $user = $this->createFactoryUser();
        $response = $this->put($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_can_not_update_user_if_email_is_null()
    {
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryUser([
            'email' => null
        ]);
        $user = $this->createFactoryUser();
        $response = $this->put($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['email']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_can_not_update_user_if_phone_number_is_null()
    {
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryUser([
            'phone_number' => null
        ]);
        $user = $this->createFactoryUser();
        $response = $this->put($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['phone_number']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_not_update_user_if_data_is_null()
    {
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryUser([
            'name' => null,
            'email' => null,
            'phone_number' => null
        ]);
        $user = $this->createFactoryUser();
        $response = $this->put($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name', 'email', 'phone_number']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_not_see_edit_form_if_user_not_exists()
    {
        $this->loginWithSuperAdmin();
        $userId = -1;
        $response = $this->get($this->getEditUserRoute($userId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_not_see_update_user_if_user_not_exists()
    {
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryUser();
        $userId = -1;
        $response = $this->put($this->getUpdateUserRoute($userId), $dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }

//    User Have Permissions

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_see_update_user_form()
    {
//        $this->withoutExceptionHandling();
        $this->loginUserWithPermissions('user-edit');
        $user = $this->createFactoryUser();
        $response = $this->get($this->getEditUserRoute($user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.edit');
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_update_user()
    {
//        $this->withoutExceptionHandling();
        $this->loginUserWithPermissions('user-update');
        $dataUpdate = $this->makeFactoryUser();
        $user = $this->createFactoryUser();
        $response = $this->put($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getListUserRoute());
        $this->assertDatabaseHas('users', $dataUpdate);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_see_text_error_if_name_is_null()
    {
        $this->loginUserWithPermissions('user-update');
        $user = $this->createFactoryUser();
        $dataUpdate = $this->makeFactoryUser([
            'name' => null
        ]);
        $user = $this->createFactoryUser();
        $response = $this->from($this->getListUserRoute($user->id))->put($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_see_text_error_if_email_is_null()
    {
        $this->loginUserWithPermissions('user-update');
        $user = $this->createFactoryUser();
        $dataUpdate = $this->makeFactoryUser([
            'email' => null
        ]);
        $user = $this->createFactoryUser();
        $response = $this->from($this->getListUserRoute($user->id))->put($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['email']);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_see_text_error_if_phone_number_is_null()
    {
        $this->loginUserWithPermissions('user-update');
        $user = $this->createFactoryUser();
        $dataUpdate = $this->makeFactoryUser([
            'phone_number' => null
        ]);
        $user = $this->createFactoryUser();
        $response = $this->from($this->getListUserRoute($user->id))->put($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['phone_number']);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_see_text_error_if_data_is_null()
    {
        $this->loginUserWithPermissions('user-update');
        $dataUpdate = $this->makeFactoryUser([
            'name' => null,
            'email' => null,
            'phone_number' => null
        ]);
        $user = $this->createFactoryUser();
        $response = $this->from($this->getListUserRoute($user->id))->put($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name', 'email', 'phone_number']);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_can_not_update_user_if_name_is_null()
    {
        $this->loginUserWithPermissions('user-update');
        $dataUpdate = $this->makeFactoryUser([
            'name' => null,
        ]);
        $user = $this->createFactoryUser();
        $response = $this->put($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_can_not_update_user_if_email_is_null()
    {
        $this->loginUserWithPermissions('user-update');
        $dataUpdate = $this->makeFactoryUser([
            'email' => null
        ]);
        $user = $this->createFactoryUser();
        $response = $this->put($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['email']);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_can_not_update_user_if_phone_number_is_null()
    {
        $this->loginUserWithPermissions('user-update');
        $dataUpdate = $this->makeFactoryUser([
            'phone_number' => null
        ]);
        $user = $this->createFactoryUser();
        $response = $this->put($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['phone_number']);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_not_update_user_if_data_is_null()
    {
        $this->loginUserWithPermissions('user-update');
        $dataUpdate = $this->makeFactoryUser([
            'name' => null,
            'email' => null,
            'phone_number' => null
        ]);
        $user = $this->createFactoryUser();
        $response = $this->put($this->getUpdateUserRoute($user->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name', 'email', 'phone_number']);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_not_see_edit_form_if_user_not_exists()
    {
        $this->loginUserWithPermissions('user-edit');
        $userId = -1;
        $response = $this->get($this->getEditUserRoute($userId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_not_see_update_user_if_user_not_exists()
    {
//        $this->withoutExceptionHandling();
        $this->loginUserWithPermissions('user-update');
        $dataUpdate = $this->makeFactoryUser();
        $userId = -1;
        $response = $this->put($this->getUpdateUserRoute($userId), $dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }

//    No Permission

    /**
     * @test
     */
    public function unauthenticated_user_can_not_see_edit_form()
    {
        $userId = 1;
        $response = $this->get($this->getEditUserRoute($userId));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));

    }

    /**
     * @test
     */
    public function unauthenticated_user_can_not_see_update_user_if_user_not_exists()
    {
        $dataUpdate = $this->makeFactoryUser();
        $userId = 1;
        $response = $this->put($this->getUpdateUserRoute($userId), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);

    }

    public function getListUserRoute()
    {
        return route('users.index');
    }

    public function getEditUserRoute($id)
    {
        return route('users.edit', $id);
    }

    public function getUpdateUserRoute($id)
    {
        return route('users.update', $id);
    }

    public function makeFactoryUser(array $customFiled = [])
    {
        return User::factory()->make($customFiled)->toArray();
    }

    public function createFactoryUser(array $customFiled = [])
    {
        return User::factory()->create($customFiled);
    }
}
