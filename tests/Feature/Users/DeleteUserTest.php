<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteUserTest extends TestCase
{
    //    Super Admin
    /**
     * @test
     */
    public function authenticated_super_admin_can_delete_user_if_user_exists()
    {
        $this->loginWithSuperAdmin();
        $user = $this->createFactoryUser();
        $userCountBefore = User::count();
        $response = $this->delete($this->getDeleteUserRoute($user->id));
        $this->assertDatabaseCount('users', $userCountBefore - 1);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_not_delete_user_if_user_not_exists()
    {
        $this->loginWithSuperAdmin();
        $userId = -1;
        $response = $this->delete($this->getDeleteUserRoute($userId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    //    User Have Permission
    /**
     * @test
     */
    public function authenticated_user_have_permission_can_delete_user_if_user_exists()
    {
        $this->loginUserWithPermissions('user-delete');
        $user = $this->createFactoryUser();
        $userCountBefore = User::count();
        $response = $this->delete($this->getDeleteUserRoute($user->id));
        $this->assertDatabaseCount('users', $userCountBefore - 1);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_not_delete_user_if_user_not_exists()
    {
        $this->loginUserWithPermissions('user-delete');
        $userId = -1;
        $response = $this->delete($this->getDeleteUserRoute($userId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_not_delete_user()
    {
        $userId = 1;
        $response = $this->delete($this->getDeleteUserRoute($userId));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    public function createFactoryUser(array $customFiled = [])
    {
        return User::factory()->create($customFiled);
    }

    public function getDeleteUserRoute($id): string
    {
        return route('users.delete', $id);
    }
}
