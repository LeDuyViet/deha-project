<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    use WithFaker;
    /**
     * @test
     */
    public function authenticated_super_admin_can_see_create_user_form()
    {
//        $this->withoutExceptionHandling();
        $this->loginWithSuperAdmin();
        $response = $this->get($this->getCreateUserRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.create');
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_new_create_user()
    {
        $this->loginWithSuperAdmin();
        $user = $this->makeFactoryUser();
        $user['password'] = bcrypt('12345678');
        $response = $this->post($this->getStoreUserRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getListUserRoute());
        $this->assertDatabaseHas('users', $user);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_see_text_error_if_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $user = $this->makeFactoryUser([
            'name' => null
        ]);
        $response = $this->from($this->getListUserRoute())->post($this->getStoreUserRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_see_text_error_if_display_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $user = $this->makeFactoryUser([
            'email' => null
        ]);
        $response = $this->from($this->getListUserRoute())->post($this->getStoreUserRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['email']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_see_text_error_if_password_is_null()
    {
        $this->loginWithSuperAdmin();
        $user = $this->makeFactoryUser([
            'password' => null
        ]);
        $response = $this->from($this->getListUserRoute())->post($this->getStoreUserRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['password']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_see_text_error_if_phone_number_is_null()
    {
        $this->loginWithSuperAdmin();
        $user = $this->makeFactoryUser([
            'phone_number' => null
        ]);
        $response = $this->from($this->getListUserRoute())->post($this->getStoreUserRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['phone_number']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_see_text_error_if_data_is_null()
    {
        $this->loginWithSuperAdmin();
        $user = $this->makeFactoryUser([
            'name' => null,
            'email' => null,
            'phone_number' => null,
            'password' => null
        ]);
        $response = $this->from($this->getListUserRoute())->post($this->getStoreUserRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name', 'email', 'phone_number', 'password']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_can_not_new_user_if_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $user = $this->makeFactoryUser([
            'name' => null,
        ]);
        $response = $this->post($this->getStoreUserRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_can_not_new_user_if_email_is_null()
    {
        $this->loginWithSuperAdmin();
        $user = $this->makeFactoryUser([
            'email' => null,
        ]);
        $response = $this->post($this->getStoreUserRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['email']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_can_not_new_user_if_password_is_null()
    {
        $this->loginWithSuperAdmin();
        $user = $this->makeFactoryUser([
            'password' => null,
        ]);
        $response = $this->post($this->getStoreUserRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['password']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_can_not_new_user_if_phone_number_is_null()
    {
        $this->loginWithSuperAdmin();
        $user = $this->makeFactoryUser([
            'phone_number' => null,
        ]);
        $response = $this->post($this->getStoreUserRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['phone_number']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_not_new_user_if_data_is_null()
    {
        $this->loginWithSuperAdmin();
        $user = $this->makeFactoryUser([
            'name' => null,
            'email' => null,
            'password' => null,
            'phone_number' => null
        ]);
        $response = $this->post($this->getStoreUserRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name', 'email', 'password', 'phone_number']);
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_see_create_user_form()
    {
        $this->loginUserWithPermissions('user-create');
        $this->loginWithSuperAdmin();
        $response = $this->get($this->getCreateUserRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('users.create');
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_new_create_user()
    {
        $this->loginUserWithPermissions('user-store');
        $user = $this->makeFactoryUser();
        $user['password'] = bcrypt('12345678');
        $response = $this->post($this->getStoreUserRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getListUserRoute());
        $this->assertDatabaseHas('users', $user);
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_see_text_error_if_name_is_null()
    {
        $this->loginUserWithPermissions('user-store');
        $user = $this->makeFactoryUser([
            'name' => null
        ]);
        $response = $this->from($this->getListUserRoute())->post($this->getStoreUserRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_see_text_error_if_email_is_null()
    {
        $this->loginUserWithPermissions('user-store');
        $user = $this->makeFactoryUser([
            'email' => null
        ]);
        $response = $this->from($this->getListUserRoute())->post($this->getStoreUserRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['email']);
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_see_text_error_if_password_is_null()
    {
        $this->loginUserWithPermissions('user-store');
        $user = $this->makeFactoryUser([
            'password' => null
        ]);
        $response = $this->from($this->getListUserRoute())->post($this->getStoreUserRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['password']);
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_see_text_error_if_phone_number_is_null()
    {
        $this->loginUserWithPermissions('user-store');
        $user = $this->makeFactoryUser([
            'phone_number' => null
        ]);
        $response = $this->from($this->getListUserRoute())->post($this->getStoreUserRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['phone_number']);
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_see_text_error_if_data_is_null()
    {
        $this->loginUserWithPermissions('user-store');
        $user = $this->makeFactoryUser([
            'name' => null,
            'email' => null,
            'password' => null,
            'phone_number' => null,
        ]);
        $response = $this->from($this->getListUserRoute())->post($this->getStoreUserRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name', 'email', 'password', 'phone_number']);
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_not_see_user_form_create()
    {
        $response = $this->get($this->getCreateUserRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_create_new_user()
    {
        $user = $this->makeFactoryUser();
        $response = $this->post($this->getStoreUserRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
    }

    public function getCreateUserRoute()
    {
        return route('users.create');
    }

    public function getListUserRoute()
    {
        return route('users.index');
    }

    public function getStoreUserRoute()
    {
        return route('users.store');
    }

    public function makeFactoryUser(array $customFile = [])
    {
        return User::factory()->make($customFile)->toArray();
    }
}
