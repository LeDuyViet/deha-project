<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetShowRoleTest extends TestCase
{
    /**
     * @test
     */
    public function authenticated_super_admin_can_get_role()
    {
        $this->loginWithSuperAdmin();
        $role = $this->createFactoryRole();
        $response = $this->get($this->getShowRoleRoute($role->id));
        $response->assertViewIs('roles.show');
        $response->assertSee($role->display_name);
        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_not_get_role_if_role_not_exists()
    {
        $this->loginWithSuperAdmin();
        $roleId = -1;
        $response = $this->get($this->getShowRoleRoute($roleId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_get_role()
    {
        $this->loginUserWithPermissions('role-show');
        $role = $this->createFactoryRole();
        $response = $this->get($this->getShowRoleRoute($role->id));
        $response->assertViewIs('roles.show');
        $response->assertSee($role->display_name);
        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_not_get_role_if_role_not_exists()
    {
        $this->loginUserWithPermissions('role-show');
        $roleId = -1;
        $response = $this->get($this->getShowRoleRoute($roleId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_not_show_role()
    {
        $role = $this->createFactoryRole();
        $response = $this->get($this->getShowRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    public function createFactoryRole(array $customFiled = [])
    {
        return Role::factory()->create($customFiled);
    }

    public function getShowRoleRoute($id)
    {
        return route('roles.show', $id);
    }
}
