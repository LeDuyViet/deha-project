<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListRolesTest extends TestCase
{
    /**
     * @test
     */
    public function authenticated_super_admin_user_can_get_list_roles()
    {
        $this->loginWithSuperAdmin();
        $role = Role::factory()->create();
        $response = $this->get(route('roles.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.index');
        $response->assertSee($role->display_name);
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_not_get_list_roles()
    {
        $response = $this->get(route('roles.index'));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_get_list_roles()
    {
        $this->loginUserWithPermissions('role-view');
        $role = Role::factory()->create();
        $response = $this->get(route('roles.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.index');
        $response->assertSee($role->display_name);

    }
}
