<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateRoleTest extends TestCase
{
//    Super Admin
    /**
     * @test
     */
    public function authenticated_super_admin_can_see_update_role_form()
    {
//        $this->withoutExceptionHandling();
        $this->loginWithSuperAdmin();
        $role = $this->createFactoryRole();
        $response = $this->get($this->getEditRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.edit');
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_update_role()
    {
//        $this->withoutExceptionHandling();
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryRole();
        $role = $this->createFactoryRole();
        $response = $this->put($this->getUpdateRoleRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getListRoleRoute());
        $this->assertDatabaseHas('roles', $dataUpdate);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_see_text_error_if_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $role = $this->createFactoryRole();
        $dataUpdate = $this->makeFactoryRole([
            'name' => null
        ]);
        $role = $this->createFactoryRole();
        $response = $this->from($this->getListRoleRoute($role->id))->put($this->getUpdateRoleRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_see_text_error_if_display_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $role = $this->createFactoryRole();
        $dataUpdate = $this->makeFactoryRole([
            'display_name' => null
        ]);
        $role = $this->createFactoryRole();
        $response = $this->from($this->getListRoleRoute($role->id))->put($this->getUpdateRoleRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['display_name']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_see_text_error_if_name_and_display_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryRole([
            'name' => null,
            'display_name' => null
        ]);
        $role = $this->createFactoryRole();
        $response = $this->from($this->getListRoleRoute($role->id))->put($this->getUpdateRoleRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name', 'display_name']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_can_not_update_role_if_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryRole([
            'name' => null,
        ]);
        $role = $this->createFactoryRole();
        $response = $this->put($this->getUpdateRoleRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_can_not_update_role_if_display_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryRole([
            'display_name' => null
        ]);
        $role = $this->createFactoryRole();
        $response = $this->put($this->getUpdateRoleRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['display_name']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_not_update_role_if_name_and_display_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryRole([
            'name' => null,
            'display_name' => null
        ]);
        $role = $this->createFactoryRole();
        $response = $this->put($this->getUpdateRoleRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name', 'display_name']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_not_see_edit_form_if_role_not_exists()
    {
        $this->loginWithSuperAdmin();
        $roleId = -1;
        $response = $this->get($this->getEditRoleRoute($roleId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_not_see_update_role_if_role_not_exists()
    {
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryRole();
        $roleId = -1;
        $response = $this->put($this->getUpdateRoleRoute($roleId), $dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }

//    User Have Permissions
    /**
     * @test
     */
    public function authenticated_user_have_permission_can_see_update_role_form()
    {
//        $this->withoutExceptionHandling();
        $this->loginUserWithPermissions('role-edit');
        $role = $this->createFactoryRole();
        $response = $this->get($this->getEditRoleRoute($role->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.edit');
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_update_role()
    {
//        $this->withoutExceptionHandling();
        $this->loginUserWithPermissions('role-update');
        $dataUpdate = $this->makeFactoryRole();
        $role = $this->createFactoryRole();
        $response = $this->put($this->getUpdateRoleRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getListRoleRoute());
        $this->assertDatabaseHas('roles', $dataUpdate);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_see_text_error_if_name_is_null()
    {
        $this->loginUserWithPermissions('role-update');
        $role = $this->createFactoryRole();
        $dataUpdate = $this->makeFactoryRole([
            'name' => null
        ]);
        $role = $this->createFactoryRole();
        $response = $this->from($this->getListRoleRoute($role->id))->put($this->getUpdateRoleRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_see_text_error_if_display_name_is_null()
    {
        $this->loginUserWithPermissions('role-update');
        $role = $this->createFactoryRole();
        $dataUpdate = $this->makeFactoryRole([
            'display_name' => null
        ]);
        $role = $this->createFactoryRole();
        $response = $this->from($this->getListRoleRoute($role->id))->put($this->getUpdateRoleRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['display_name']);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_see_text_error_if_name_and_display_name_is_null()
    {
        $this->loginUserWithPermissions('role-update');
        $dataUpdate = $this->makeFactoryRole([
            'name' => null,
            'display_name' => null
        ]);
        $role = $this->createFactoryRole();
        $response = $this->from($this->getListRoleRoute($role->id))->put($this->getUpdateRoleRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name', 'display_name']);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_can_not_update_role_if_name_is_null()
    {
        $this->loginUserWithPermissions('role-update');
        $dataUpdate = $this->makeFactoryRole([
            'name' => null,
        ]);
        $role = $this->createFactoryRole();
        $response = $this->put($this->getUpdateRoleRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_can_not_update_role_if_display_name_is_null()
    {
        $this->loginUserWithPermissions('role-update');
        $dataUpdate = $this->makeFactoryRole([
            'display_name' => null
        ]);
        $role = $this->createFactoryRole();
        $response = $this->put($this->getUpdateRoleRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['display_name']);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_not_update_role_if_name_and_display_name_is_null()
    {
        $this->loginUserWithPermissions('role-update');
        $dataUpdate = $this->makeFactoryRole([
            'name' => null,
            'display_name' => null
        ]);
        $role = $this->createFactoryRole();
        $response = $this->put($this->getUpdateRoleRoute($role->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name', 'display_name']);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_not_see_edit_form_if_role_not_exists()
    {
        $this->loginUserWithPermissions('role-edit');
        $roleId = -1;
        $response = $this->get($this->getEditRoleRoute($roleId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_not_see_update_role_if_role_not_exists()
    {
//        $this->withoutExceptionHandling();
        $this->loginUserWithPermissions('role-update');
        $dataUpdate = $this->makeFactoryRole();
        $roleId = -1;
        $response = $this->put($this->getUpdateRoleRoute($roleId), $dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }

//    No Permission
    /**
     * @test
     */
    public function unauthenticated_user_can_not_see_edit_form()
    {
        $roleId = 1;
        $response = $this->get($this->getEditRoleRoute($roleId));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));

    }

    /**
     * @test
     */
    public function unauthenticated_user_can_not_see_update_role_if_role_not_exists()
    {
        $dataUpdate = $this->makeFactoryRole();
        $roleId = 1;
        $response = $this->put($this->getUpdateRoleRoute($roleId), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);

    }

    public function getListRoleRoute()
    {
        return route('roles.index');
    }

    public function getEditRoleRoute($id)
    {
        return route('roles.edit', $id);
    }

    public function getUpdateRoleRoute($id)
    {
        return route('roles.update', $id);
    }

    public function makeFactoryRole(array $customFiled = [])
    {
        return Role::factory()->make($customFiled)->toArray();
    }

    public function createFactoryRole(array $customFiled = [])
    {
        return Role::factory()->create($customFiled);
    }
}
