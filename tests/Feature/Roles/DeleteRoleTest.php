<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteRoleTest extends TestCase
{
//    Super Admin
    /**
     * @test
     */
    public function authenticated_super_admin_can_delete_role_if_role_exists()
    {
        $this->loginWithSuperAdmin();
        $role = $this->createFactoryRole();
        $roleCountBefore = Role::count();
        $response = $this->delete($this->getDeleteRoleRoute($role->id));
        $this->assertDatabaseCount('roles', $roleCountBefore - 1);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_not_delete_role_if_role_not_exists()
    {
        $this->loginWithSuperAdmin();
        $roleId = -1;
        $response = $this->delete($this->getDeleteRoleRoute($roleId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    //    User Have Permission
    /**
     * @test
     */
    public function authenticated_user_have_permission_can_delete_role_if_role_exists()
    {
        $this->loginUserWithPermissions('role-delete');
        $role = $this->createFactoryRole();
        $roleCountBefore = Role::count();
        $response = $this->delete($this->getDeleteRoleRoute($role->id));
        $this->assertDatabaseCount('roles', $roleCountBefore - 1);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_not_delete_role_if_role_not_exists()
    {
        $this->loginUserWithPermissions('role-delete');
        $roleId = -1;
        $response = $this->delete($this->getDeleteRoleRoute($roleId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_not_delete_role()
    {
        $roleId = 1;
        $response = $this->delete($this->getDeleteRoleRoute($roleId));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    public function createFactoryRole(array $customFiled = [])
    {
        return Role::factory()->create($customFiled);
    }

    public function getDeleteRoleRoute($id): string
    {
        return route('roles.delete', $id);
    }
}
