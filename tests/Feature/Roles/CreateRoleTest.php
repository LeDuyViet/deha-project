<?php

namespace Tests\Feature\Roles;

use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateRoleTest extends TestCase
{
    /**
     * @test
     */
    public function authenticated_super_admin_can_see_create_role_form()
    {
//        $this->withoutExceptionHandling();
        $this->loginWithSuperAdmin();
        $response = $this->get($this->getCreateRoleRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.create');
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_new_create_role()
    {
        $this->loginWithSuperAdmin();
        $role = $this->makeFactoryRole();
        $response = $this->post($this->getStoreRoleRoute(), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getListRoleRoute());
        $this->assertDatabaseHas('roles', $role);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_see_text_error_if_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $role = $this->makeFactoryRole([
            'name' => null
        ]);
        $response = $this->from($this->getListRoleRoute())->post($this->getStoreRoleRoute(), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_see_text_error_if_display_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $role = $this->makeFactoryRole([
            'display_name' => null,
        ]);
        $response = $this->from($this->getListRoleRoute())->post($this->getStoreRoleRoute(), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['display_name']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_see_text_error_if_name_and_display_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $role = $this->makeFactoryRole([
            'name' => null,
            'display_name' => null
        ]);
        $response = $this->from($this->getListRoleRoute())->post($this->getStoreRoleRoute(), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name', 'display_name']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_can_not_new_role_if_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $role = $this->makeFactoryRole([
            'name' => null,
        ]);
        $response = $this->post($this->getStoreRoleRoute(), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_can_not_new_role_if_display_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $role = $this->makeFactoryRole([
            'display_name' => null
        ]);
        $response = $this->post($this->getStoreRoleRoute(), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['display_name']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_not_new_role_if_name_and_display_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $role = $this->makeFactoryRole([
            'name' => null,
            'display_name' => null
        ]);
        $response = $this->post($this->getStoreRoleRoute(), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name', 'display_name']);
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_see_create_role_form()
    {
        $this->loginUserWithPermissions('role-create');
        $this->loginWithSuperAdmin();
        $response = $this->get($this->getCreateRoleRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('roles.create');
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_new_create_role()
    {
        $this->loginUserWithPermissions('role-store');
        $role = $this->makeFactoryRole();
        $response = $this->post($this->getStoreRoleRoute(), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getListRoleRoute());
        $this->assertDatabaseHas('roles', $role);
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_see_text_error_if_name_is_null()
    {
        $this->loginUserWithPermissions('role-store');
        $role = $this->makeFactoryRole([
            'name' => null
        ]);
        $response = $this->from($this->getListRoleRoute())->post($this->getStoreRoleRoute(), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_see_text_error_if_display_name_is_null()
    {
        $this->loginUserWithPermissions('role-store');
        $role = $this->makeFactoryRole([
            'display_name' => null
        ]);
        $response = $this->from($this->getListRoleRoute())->post($this->getStoreRoleRoute(), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['display_name']);
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_see_text_error_if_name_and_display_name_is_null()
    {
        $this->loginUserWithPermissions('role-store');
        $role = $this->makeFactoryRole([
            'name' => null,
            'display_name' => null
        ]);
        $response = $this->from($this->getListRoleRoute())->post($this->getStoreRoleRoute(), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name', 'display_name']);
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_can_not_new_role_if_name_is_null()
    {
        $this->loginUserWithPermissions('role-store');
        $role = $this->makeFactoryRole([
            'name' => null,
        ]);
        $response = $this->post($this->getStoreRoleRoute(), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_can_not_new_role_if_display_name_is_null()
    {
        $this->loginUserWithPermissions('role-store');
        $role = $this->makeFactoryRole([
            'display_name' => null
        ]);
        $response = $this->post($this->getStoreRoleRoute(), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['display_name']);
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_not_new_role_if_name_and_display_name_is_null()
    {
        $this->loginUserWithPermissions('role-store');
        $role = $this->makeFactoryRole([
            'name' => null,
            'display_name' => null
        ]);
        $response = $this->post($this->getStoreRoleRoute(), $role);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name', 'display_name']);
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_not_see_role_form_create()
    {
        $response = $this->get($this->getCreateRoleRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_create_new_role()
    {
        $role = $this->makeFactoryRole();
        $response = $this->post($this->getStoreRoleRoute(), $role);
        $response->assertStatus(Response::HTTP_FOUND);
    }

    public function getCreateRoleRoute()
    {
        return route('roles.create');
    }

    public function getListRoleRoute()
    {
        return route('roles.index');
    }

    public function getStoreRoleRoute()
    {
        return route('roles.store');
    }

    public function makeFactoryRole(array $customFile = [])
    {
        return Role::factory()->make($customFile)->toArray();
    }
}
