<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateCategoriesTest extends TestCase
{
    /**
     * @test
     */
    public function authenticated_super_admin_can_see_create_category_form()
    {
//        $this->withoutExceptionHandling();
        $this->loginWithSuperAdmin();
        $response = $this->get($this->getCreateCategoryRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.create');
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_new_create_category()
    {
        $this->loginWithSuperAdmin();
        $category = $this->makeFactoryCategory();
        $response = $this->post($this->getStoreCategoryRoute(), $category);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getListCategoryRoute());
        $this->assertDatabaseHas('categories', $category);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_see_text_error_if_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $category = $this->makeFactoryCategory([
            'name' => null
        ]);
        $response = $this->from($this->getListCategoryRoute())->post($this->getStoreCategoryRoute(), $category);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_can_not_new_category_if_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $category = $this->makeFactoryCategory([
            'name' => null,
        ]);
        $response = $this->post($this->getStoreCategoryRoute(), $category);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_see_create_category_form()
    {
        $this->loginUserWithPermissions('category-create');
        $this->loginWithSuperAdmin();
        $response = $this->get($this->getCreateCategoryRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.create');
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_new_create_category()
    {
        $this->loginUserWithPermissions('category-store');
        $category = $this->makeFactoryCategory();
        $response = $this->post($this->getStoreCategoryRoute(), $category);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getListCategoryRoute());
        $this->assertDatabaseHas('categories', $category);
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_see_text_error_if_name_is_null()
    {
        $this->loginUserWithPermissions('category-store');
        $category = $this->makeFactoryCategory([
            'name' => null
        ]);
        $response = $this->from($this->getListCategoryRoute())->post($this->getStoreCategoryRoute(), $category);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_can_not_new_category_if_name_is_null()
    {
        $this->loginUserWithPermissions('category-store');
        $category = $this->makeFactoryCategory([
            'name' => null,
        ]);
        $response = $this->post($this->getStoreCategoryRoute(), $category);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_not_see_category_form_create()
    {
        $response = $this->get($this->getCreateCategoryRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_create_new_category()
    {
        $category = $this->makeFactoryCategory();
        $response = $this->post($this->getStoreCategoryRoute(), $category);
        $response->assertStatus(Response::HTTP_FOUND);
    }

    public function getCreateCategoryRoute()
    {
        return route('categories.create');
    }

    public function getListCategoryRoute()
    {
        return route('categories.index');
    }

    public function getStoreCategoryRoute()
    {
        return route('categories.store');
    }

    public function makeFactoryCategory(array $customFile = [])
    {
        return Category::factory()->make($customFile)->toArray();
    }

}
