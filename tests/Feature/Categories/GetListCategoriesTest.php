<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListCategoriesTest extends TestCase
{
    /**
     * @test
     */
    public function authenticated_super_admin_user_can_get_list_categories()
    {
        $this->loginWithSuperAdmin();
        $category = Category::factory()->create();
        $response = $this->get(route('categories.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.index');
        $response->assertSee($category->name);
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_not_get_list_categories()
    {
        $response = $this->get(route('categories.index'));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /**
     * @test
     */
    public function authenticated_have_permission_can_get_list_categories()
    {
        $this->loginUserWithPermissions('category-view');
        $category = Category::factory()->create();
        $response = $this->get(route('categories.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.index');
        $response->assertSee($category->name);

    }
}
