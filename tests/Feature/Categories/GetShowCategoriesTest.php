<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetShowCategoriesTest extends TestCase
{
    /**
     * @test
     */
    public function authenticated_super_admin_can_get_category()
    {
        $this->loginWithSuperAdmin();
        $category = $this->createFactoryCategory();
        $response = $this->get($this->getShowCategoryRoute($category->id));
        $response->assertViewIs('categories.show');
        $response->assertSee($category->name);
        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_not_get_category_if_category_not_exists()
    {
        $this->loginWithSuperAdmin();
        $categoryId = -1;
        $response = $this->get($this->getShowCategoryRoute($categoryId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_get_category()
    {
        $this->loginUserWithPermissions('category-show');
        $category = $this->createFactoryCategory();
        $response = $this->get($this->getShowCategoryRoute($category->id));
        $response->assertViewIs('categories.show');
        $response->assertSee($category->name);
        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_not_get_category_if_category_not_exists()
    {
        $this->loginUserWithPermissions('category-show');
        $categoryId = -1;
        $response = $this->get($this->getShowCategoryRoute($categoryId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_not_show_category()
    {
        $category = $this->createFactoryCategory();
        $response = $this->get($this->getShowCategoryRoute($category->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    public function createFactoryCategory(array $customFiled = [])
    {
        return Category::factory()->create($customFiled);
    }

    public function getShowCategoryRoute($id)
    {
        return route('categories.show', $id);
    }
}
