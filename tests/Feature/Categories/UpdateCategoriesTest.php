<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateCategoriesTest extends TestCase
{
    //    Super Admin
    /**
     * @test
     */
    public function authenticated_super_admin_can_see_update_category_form()
    {
        $this->withoutExceptionHandling();
        $this->loginWithSuperAdmin();
        $category = $this->createFactoryCategory();
        $response = $this->get($this->getEditCategoryRoute($category->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.edit');
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_update_category()
    {
//        $this->withoutExceptionHandling();
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryCategory();
        $category = $this->createFactoryCategory();
        $response = $this->put($this->getUpdateCategoryRoute($category->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getListCategoryRoute());
        $this->assertDatabaseHas('categories', $dataUpdate);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_see_text_error_if_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $category = $this->createFactoryCategory();
        $dataUpdate = $this->makeFactoryCategory([
            'name' => null
        ]);
        $category = $this->createFactoryCategory();
        $response = $this->from($this->getListCategoryRoute($category->id))->put($this->getUpdateCategoryRoute($category->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }


    /**
     * @test
     */
    public function authenticated_super_admin_can_can_not_update_category_if_name_is_null()
    {
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryCategory([
            'name' => null,
        ]);
        $category = $this->createFactoryCategory();
        $response = $this->put($this->getUpdateCategoryRoute($category->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }


    /**
     * @test
     */
    public function authenticated_super_admin_can_not_see_edit_form_if_category_not_exists()
    {
        $this->loginWithSuperAdmin();
        $categoryId = -1;
        $response = $this->get($this->getEditCategoryRoute($categoryId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_not_see_update_category_if_category_not_exists()
    {
        $this->loginWithSuperAdmin();
        $dataUpdate = $this->makeFactoryCategory();
        $categoryId = -1;
        $response = $this->put($this->getUpdateCategoryRoute($categoryId), $dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }

//    User Have Permissions

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_see_update_category_form()
    {
//        $this->withoutExceptionHandling();
        $this->loginUserWithPermissions('category-edit');
        $category = $this->createFactoryCategory();
        $response = $this->get($this->getEditCategoryRoute($category->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.edit');
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_update_category()
    {
//        $this->withoutExceptionHandling();
        $this->loginUserWithPermissions('category-update');
        $dataUpdate = $this->makeFactoryCategory();
        $category = $this->createFactoryCategory();
        $response = $this->put($this->getUpdateCategoryRoute($category->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->getListCategoryRoute());
        $this->assertDatabaseHas('categories', $dataUpdate);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_see_text_error_if_name_is_null()
    {
        $this->loginUserWithPermissions('category-update');
        $category = $this->createFactoryCategory();
        $dataUpdate = $this->makeFactoryCategory([
            'name' => null
        ]);
        $category = $this->createFactoryCategory();
        $response = $this->from($this->getListCategoryRoute($category->id))->put($this->getUpdateCategoryRoute($category->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }


    /**
     * @test
     */
    public function authenticated_user_have_permission_can_can_not_update_category_if_name_is_null()
    {
        $this->loginUserWithPermissions('category-update');
        $dataUpdate = $this->makeFactoryCategory([
            'name' => null,
        ]);
        $category = $this->createFactoryCategory();
        $response = $this->put($this->getUpdateCategoryRoute($category->id), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name']);
    }


    /**
     * @test
     */
    public function authenticated_user_have_permission_can_not_see_edit_form_if_category_not_exists()
    {
        $this->loginUserWithPermissions('category-edit');
        $categoryId = -1;
        $response = $this->get($this->getEditCategoryRoute($categoryId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_not_see_update_category_if_category_not_exists()
    {
//        $this->withoutExceptionHandling();
        $this->loginUserWithPermissions('category-update');
        $dataUpdate = $this->makeFactoryCategory();
        $categoryId = -1;
        $response = $this->put($this->getUpdateCategoryRoute($categoryId), $dataUpdate);
        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }

//    No Permission

    /**
     * @test
     */
    public function unauthenticated_user_can_not_see_edit_form()
    {
        $categoryId = 1;
        $response = $this->get($this->getEditCategoryRoute($categoryId));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));

    }

    /**
     * @test
     */
    public function unauthenticated_user_can_not_see_update_category_if_category_not_exists()
    {
        $dataUpdate = $this->makeFactoryCategory();
        $categoryId = 1;
        $response = $this->put($this->getUpdateCategoryRoute($categoryId), $dataUpdate);
        $response->assertStatus(Response::HTTP_FOUND);

    }

    public function getListCategoryRoute()
    {
        return route('categories.index');
    }

    public function getEditCategoryRoute($id)
    {
        return route('categories.edit', $id);
    }

    public function getUpdateCategoryRoute($id)
    {
        return route('categories.update', $id);
    }

    public function makeFactoryCategory(array $customFiled = [])
    {
        return Category::factory()->make($customFiled)->toArray();
    }

    public function createFactoryCategory(array $customFiled = [])
    {
        return Category::factory()->create($customFiled);
    }

}
