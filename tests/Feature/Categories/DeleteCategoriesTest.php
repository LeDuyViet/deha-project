<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteCategoriesTest extends TestCase
{
    //    Super Admin
    /**
     * @test
     */
    public function authenticated_super_admin_can_delete_category_if_category_exists()
    {
        $this->loginWithSuperAdmin();
        $category = $this->createFactoryCategory();
        $categoryCountBefore = Category::count();
        $response = $this->delete($this->getDeleteCategoryRoute($category->id));
        $this->assertDatabaseCount('categories', $categoryCountBefore - 1);
    }

    /**
     * @test
     */
    public function authenticated_super_admin_can_not_delete_category_if_category_not_exists()
    {
        $this->loginWithSuperAdmin();
        $categoryId = -1;
        $response = $this->delete($this->getDeleteCategoryRoute($categoryId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    //    User Have Permission
    /**
     * @test
     */
    public function authenticated_user_have_permission_can_delete_category_if_category_exists()
    {
        $this->loginUserWithPermissions('category-delete');
        $category = $this->createFactoryCategory();
        $categoryCountBefore = Category::count();
        $response = $this->delete($this->getDeleteCategoryRoute($category->id));
        $this->assertDatabaseCount('categories', $categoryCountBefore - 1);
    }

    /**
     * @test
     */
    public function authenticated_user_have_permission_can_not_delete_category_if_category_not_exists()
    {
        $this->loginUserWithPermissions('category-delete');
        $categoryId = -1;
        $response = $this->delete($this->getDeleteCategoryRoute($categoryId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /**
     * @test
     */
    public function unauthenticated_user_can_not_delete_category()
    {
        $categoryId = 1;
        $response = $this->delete($this->getDeleteCategoryRoute($categoryId));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    public function createFactoryCategory(array $customFiled = [])
    {
        return Category::factory()->create($customFiled);
    }

    public function getDeleteCategoryRoute($id): string
    {
        return route('categories.delete', $id);
    }
}
