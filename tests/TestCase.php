<?php

namespace Tests;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseTransactions;

    protected function loginWithSuperAdmin()
    {
        $user = User::factory()->create();
        $superAdminRole = Role::where('name', 'like', '%super_admin%')->pluck('id');
        $user->roles()->attach($superAdminRole);
        return $this->actingAs($user);
    }

    protected function loginUserWithPermissions($permission)
    {
        $user = User::factory()->create();
        $role = Role::factory()->create();
        $user->roles()->attach($role);
        $permission = Permission::where('name', $permission)->first();
        $role->permissions()->attach($permission);
        $this->actingAs($user);
    }

    protected function loginWithUser()
    {
        return $this->actingAs(User::factory()->create());
    }
}
