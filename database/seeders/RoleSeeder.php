<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::factory(10)->create();
        $roles = Role::factory(10)->create();
        $users->each(function ($user) use ($roles){
            $user->roles()->attach($roles->random(rand(1,2)));
        });

        $permissions = Permission::all();
        $roles->each(function ($role) use ($permissions){
            $role->permissions()->attach($permissions->random(rand(5, 12)));
        });


    }
}
