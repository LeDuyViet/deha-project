<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $parentCategoriesData = ['Adidas', 'Nike', 'Puma'];
        $categoriesData = ['Shirts', 'Jean', 'T-Shirts', 'Shoes', 'Jackets', 'Holiday', 'Workwears', 'Slim fit', 'Shocks', 'gloves', 'Handkerchief', 'Underwear', 'Hat'];

        $dataParent = [];
        foreach($parentCategoriesData as $category) {
            $dataParent[] = ['name' => $category];
        }

        $dataChildren = [];
        foreach($categoriesData as $category) {
            $dataChildren[] = ['name' => $category, 'parent_id' => rand(1, 3)];
        }

        $categories = Category::insert($dataParent);
        $categories = Category::insert($dataChildren);
    }
}
