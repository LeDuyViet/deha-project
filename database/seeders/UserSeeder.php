<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        Super Admin
        $user = User::create([
            'name' => 'Viet',
            'email' => 'admin@gmail.com',
            'birthday' => '2000/12/1',
            'address' => 'Thai Nguyen',
            'phone_number' => '0912345678',
            'gender' => '0',
            'password' => bcrypt(12345678)
        ]);
        $role = Role::create([
            'name' => 'super-admin',
            'display_name' => 'Super Admin',
        ]);
        $user->roles()->attach($role->id);
//        Member User
        $user = User::create([
            'name' => 'Viet',
            'email' => 'user@gmail.com',
            'birthday' => '2000/12/1',
            'address' => 'Thai Nguyen',
            'phone_number' => '0912345678',
            'gender' => '0',
            'password' => bcrypt(12345678)
        ]);
        $role = Role::create([
            'name' => 'member-user',
            'display_name' => 'Member User',
        ]);
        $user->roles()->attach($role->id);
    }
}
