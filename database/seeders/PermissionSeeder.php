<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $permissionsName = ['User View',
//            'User Create',
//            'User Store',
//            'User Edit',
//            'User Update',
//            'User Delete',
//            'User Show',
//            'Role View',
//            'Role Create',
//            'Role Store',
//            'Role Edit',
//            'Role Update',
//            'Role Delete',
//            'Role Show',
//            'Product View',
//            'Product Create',
//            'Product Store',
//            'Product Edit',
//            'Product Update',
//            'Product Delete',
//            'Product Show',
//            'Category View',
//            'Category Create',
//            'Category Store',
//            'Category Edit',
//            'Category Update',
//            'Category Delete',
//            'Category Show',
//        ];
        $permissionNames = ['View',
            'Create',
            'Store',
            'Edit',
            'Update',
            'Delete',
            'Show'];
        $groupNames = ['User',
            'Role',
            'Product',
            'Category'];
        $permissions = [];
        foreach ($groupNames as $groupName) {
            foreach ($permissionNames as $permission) {
                $permissions[] = ['name' => Str::slug($groupName . ' ' . $permission, '-'), 'display_name' => $groupName . ' ' . $permission, 'group_name' => $groupName];
            }
        }

        Permission::insert($permissions);

    }
}
