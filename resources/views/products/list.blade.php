<div class="row">
    <div class="col-sm-12">
        <table
            class="table table-centered table-striped dt-responsive nowrap w-100 dataTable no-footer dtr-inline"
            id="products-datatable" role="grid" aria-describedby="products-datatable_info"
            style="width: 1554px;">
            <thead>
            <tr role="row">
                <th tabindex="0" aria-controls="products-datatable" rowspan="1" colspan="1"
                    aria-label="Customer: activate to sort column ascending" style="min-width: 200px;">
                    {{ucfirst('name')}}
                </th>
                <th tabindex="0" aria-controls="products-datatable" rowspan="1" colspan="1"

                    aria-label="Phone: activate to sort column ascending">{{ucfirst('price')}}
                </th>
                <th class="sorting_disabled" rowspan="1" colspan="1"
                    aria-label="Action"> {{ucfirst('category')}}
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                @forelse($products as $product)
                    <td class="table-user">
                        <img src="{{ $product->image }}" alt="table-user"
                             class="me-2 rounded-circle">
                        <a data-action="{{ route('products.show', $product->id) }}"
                           class="btn-show text-title text-body fw-semibold" data-toggle="modal" data-bs-toggle="modal"
                           data-bs-target="#modal-edit-product">{{ $product->name }}</a>
                    </td>
                    <td>
                        ${{ number_format($product->price, 2) }}
                    </td>
                    <td>
                        {{--                                    {{ $product->gender }}--}}
                        @foreach($product->categories()->get() as $category)
                            {{--                                        <option {{ request()->category_id == $category->id ? 'selected' : '' }} value="{{ $category->id }}"></option>--}}
                            <span class="badge bg-success">{{ $category->name }}</span>
                        @endforeach
                    </td>
                    <td class="d-flex justify-content-sm-evenly align-items-center">
                        <div class="justify-content-center d-flex">
                            @hasPermission('product-edit')
                            <button data-action="{{ route('products.edit', $product->id) }}"
                                    class="btn btn-warning mx-1 btn-edit" data-toggle="modal" data-bs-toggle="modal"
                                    data-bs-target="#modal-edit-product"><i class="mdi mdi-wrench"></i>
                            </button>
                            @endhasPermission
                            @hasPermission('product-delete')
                            <button data-action="{{ route('products.delete', $product->id) }}"
                                    class="btn btn-danger btn-delete"><i
                                    class="mdi mdi-window-close"></i>
                            </button>
                            @endhasPermission
                        </div>
                    </td>
            </tr>
            @empty
                <tbody class="">
                <td colspan="8" class="text-center"><h1 class="">No matching products found</h1></td>
                </tbody>
            @endforelse
                </tbody>
        </table>
    </div>
</div>
{{ $products->appends(request()->all())->links() }}

