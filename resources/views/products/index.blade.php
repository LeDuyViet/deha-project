@extends('layouts.app')

@section('content')
    @if(Session::has('message'))
        <p class="alert alert-info">{{ Session('message') }}</p>
    @endif
    <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('products.index') }}">Product</a></li>
                                <li class="breadcrumb-item active">Product List</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Product</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            @hasPermission('product-create')
            <div class="row mb-2">
                <div class="col-sm-2">
                    <button type="button" data-bs-toggle="modal" data-bs-target="#modal-create-product"
                            class="btn btn-success btn-rounded mb-3 btn-create"><i
                            class="mdi mdi-plus" data-toggle="modal"></i>Create
                    </button>
                </div>
                <div class="col-sm-8 app-search dropdown d-none d-lg-block">
                    <form method="GET" class="form-search">
                        <div class="input-group">
                            <input value="{{ request()->name }}" name="name" type="text" autocomplete="off"
                                   class="form-control search-field" placeholder="Search..." id="top-search" style="padding-left: 10px; background-color: #fff; border: 1px solid #e7ebf0;">
                        </div>
                        <div class=" form-group mt-2 w-100" id="search-dropdown">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="dropdown-header noti-title">
                                        <h6 class="text-overflow text-uppercase">Category</h6>
                                        <select class="form-control select2" data-toggle="select2" name="category" style="padding-left: 10px; background-color: #fff; border: 1px solid #e7ebf0;">
                                            <option value="">Select</option>
                                            <optgroup>
                                                @foreach($categories as $category)
                                                    <option
                                                        {{ request()->category == $category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                                                @endforeach
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="dropdown-header noti-title">
                                        <h6 class="text-overflow text-uppercase">Min Price</h6>
                                        <input value="" name="min_price" type="text" autocomplete="off"
                                               class="form-control search-field" placeholder="Min Price"
                                               id="top-search" style="padding-left: 10px; background-color: #fff; border: 1px solid #e7ebf0;">
                                    </div>

                                </div>
                                <div class="col-sm-4">
                                    <div class="dropdown-header noti-title">
                                        <h6 class="text-overflow text-uppercase">Max Price</h6>
                                        <input value="" name="max_price" type="text" autocomplete="off"
                                               class="form-control search-field" placeholder="Max Price"
                                               id="top-search" style="padding-left: 10px; background-color: #fff; border: 1px solid #e7ebf0;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @endhasPermission
            <!-- start row-->
            @include('products.create')
            @include('products.edit')
            <div id="list" data-action="{{ route('products.list') }}">
            </div>
            {{--Edit Product--}}
            <!-- end row-->
        </div> <!-- container -->
    </div>
@endsection
@push('js')
    <script src="{{ asset('admin/js/handleUploadImage.js')}}" defer></script>
    <script src="{{ asset('admin/js/product.js')}}" defer></script>
@endpush
