<div class="modal fade" id="modal-edit-product" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-normal text-dark" id="exampleModalLabel">
                    Update New Product</h5>
                <button type="button" class="btn-close text-dark" data-bs-dismiss="modal"
                        aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form id="form-edit-product" data-action=""
                      enctype="multipart/form-data">
                    <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
                        @csrf
                        <div class="form-group custom-form-group-create">
                            <strong class="form-check">Name: </strong>
                            <div class="form-check input-group input-group-dynamic info-horizontal">
                                <input type="text" name="name" id="name_edit" class="form-control form-edit shadow-none"
                                       style="outline: none">
                            </div>
                            <div style="display: none"
                                 class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show  mx-3 my-1 name_error errors"></div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
                        <div class="form-group custom-form-group-create">
                            <strong class="form-check">Price: </strong>
                            <div class="form-check input-group input-group-dynamic info-horizontal">
                                <input type="text" name="price" id="price_edit" class="form-control form-edit shadow-none"
                                       style="outline: none">
                            </div>
                            <div style="display: none"
                                 class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show  mx-3 my-1 price_error errors"></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
                        <div class="form-group custom-form-group-create">
                            <strong class="form-check">Description: </strong>
                            <div class="form-check input-group input-group-dynamic info-horizontal">
                                <textarea type="text" name="description" id="description_edit" class="form-control form-edit shadow-none description-abc"
                                       style="outline: none"></textarea>
                            </div>
                            <div style="display: none"
                                 class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show  mx-3 my-1 description_error errors"></div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 mt-3">
                        <div class="form-group custom-form-group-create">
                            <strong class="form-check">Image: </strong>

                            <div class="form-check input-group input-group-dynamic info-horizontal">
                                <input class="form-control form-edit image-input" name="image" type="file" id="image">
                            </div>
                            <div style="display: none"
                                 class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show  mx-3 my-1 image_error errors"></div>
                        </div>
                        <!-- Preview -->
                        <div class="dropzone-previews mx-3 mt-3 position-relative" id="file-previews"
                             >
                            <button type="button" class="btn-close close-image position-absolute"
                                    style="top: 2%; left: 0.5%;" aria-label="Close"></button>
                            <img class="image_edit rounded bg-light image-data img-thumbnail .img-fluid"
                                 style="max-width: 30%; height: auto;"/>
                        </div>
                    </div>
                    <div class="form-group mx-3">
                        <label for="" class="text-lg text-bold text-dark mt-2 font-14 my-3">Category</label>
                        <div class="h-25 d-flex flex-wrap justify-content-sm-between">
                            @foreach($categories as $category)
                                <div class="form-check mb-2 w-25">
                                    <input type="checkbox" class="form-check-edit form-edit" name="category_ids[]" id="{{ $category->id  }}" value="{{ $category->id }}">
                                    <label class="form-check-label" for="{{ $category->id  }}">{{ $category->name }}</label>
                                </div>
                            @endforeach
                        </div>
                        <div style="display: none"
                             class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show my-1 category_ids_error errors"></div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn bg-gradient-secondary btn-warning" data-bs-dismiss="modal">
                    Close
                </button>
                <button type="button" class="btn bg-gradient-primary btn-update-product btn-success">Update</button>
            </div>
        </div>
    </div>
</div>
