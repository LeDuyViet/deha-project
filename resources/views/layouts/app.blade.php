﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Dashboard | Hyper - Responsive Bootstrap 5 Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description">
    <meta content="Coderthemes" name="author">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}">

    <!-- third party css -->
    <link href="{{ asset('assets/css/vendor/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet" type="text/css">
    <!-- third party css end -->

    <!-- App css -->
    <link href="{{ asset('assets/css/icons.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/app.min.css') }}" rel="stylesheet" type="text/css" id="light-style">
    <link href="{{ asset('assets/css/app-dark.min.css') }}" rel="stylesheet" type="text/css" id="dark-style">

</head>

<body class="loading"
    data-layout-config='{"leftSideBarTheme":"dark","layoutBoxed":false, "leftSidebarCondensed":false, "leftSidebarScrollable":false,"darkMode":false, "showRightSidebarOnStart": true}'>
    <!-- Begin page -->
    <div class="wrapper">
        <!-- ========== Left Sidebar Start ========== -->
        <div class="leftside-menu">

            <!-- LOGO -->
            <a href="{{ route('dashboard') }}" class="logo text-center logo-light mt-2 mb-3">
                <span class="logo-lg">
                    <img src="{{ asset('logo.png') }}" alt="" height="80">
                </span>
                <span class="logo-sm">
                    <img src="{{ asset('assets/images/logo_sm.png') }}" alt="" height="16">
                </span>
            </a>

            <!-- LOGO -->
            <a href="{{ route('dashboard') }}" class="logo text-center logo-dark">
                <span class="logo-lg">
                    <img src="{{ asset('assets/images/logo-dark.png') }}" alt="" height="16">
                </span>
                <span class="logo-sm">
                    <img src="{{ asset('assets/images/logo_sm_dark.png') }}" alt="" height="16">
                </span>
            </a>

            <div class="h-100 mt-3" id="leftside-menu-container" data-simplebar="">
                <!--- Sidemenu -->
                @hasPermission('role-view')
                <ul class="side-nav">
                    <li class="side-nav-item {{ request()->is('roles/*') ? 'menuitem-active' : '' }}">
                        <a href="{{ route('roles.index') }}" class="side-nav-link">
                            <i class="uil-box"></i>
                            <span>Role</span>
                        </a>
                    </li>
                </ul>
                @endhasPermission
                <!--- Sidemenu -->
                @hasPermission('user-view')
                <ul class="side-nav">
                    <li class="side-nav-item {{ request()->is('users/*') ? 'menuitem-active' : '' }}">
                        <a href="{{ route('users.index') }}" class="side-nav-link">
                            <i class="uil-user"></i>
                            <span>User</span>
                        </a>
                    </li>
                </ul>
                @endhasPermission
                @hasPermission('category-view')
                <ul class="side-nav">
                    <li class="side-nav-item {{ request()->is('categories/*') ? 'menuitem-active' : '' }}">
                        <a href="{{ route('categories.index') }}" class="side-nav-link">
                            <i class="uil-book"></i>
                            <span>Category</span>
                        </a>
                    </li>
                </ul>
                @endhasPermission
                @hasPermission('category-view')
                <ul class="side-nav">
                    <li class="side-nav-item {{ request()->is('products/*') ? 'menuitem-active' : '' }}">
                        <a href="{{ route('products.index') }}" class="side-nav-link">
                            <i class="uil-calender"></i>
                            <span>Product</span>
                        </a>
                    </li>
                </ul>
                @endhasPermission
                {{--  --}}
                @hasPermission('category-view')
                <ul class="side-nav">
                    <li class="side-nav-item {{ request()->is('products/*') ? 'menuitem-active' : '' }}">
                        <a href="{{ route('products.index') }}" class="side-nav-link">
                            <i class="uil-shopping-cart-alt"></i>
                            <span>Cart</span>
                        </a>
                    </li>
                </ul>
                @endhasPermission
                @hasPermission('category-view')
                <ul class="side-nav">
                    <li class="side-nav-item {{ request()->is('products/*') ? 'menuitem-active' : '' }}">
                        <a href="{{ route('products.index') }}" class="side-nav-link">
                            <i class="uil-postcard"></i>
                            <span>Posts</span>
                        </a>
                    </li>
                </ul>
                @endhasPermission
                @hasPermission('category-view')
                <ul class="side-nav">
                    <li class="side-nav-item {{ request()->is('products/*') ? 'menuitem-active' : '' }}">
                        <a href="{{ route('products.index') }}" class="side-nav-link">
                            <i class="mdi mdi-wallet-giftcard "></i>
                            <span>Sales</span>
                        </a>
                    </li>
                </ul>
                @endhasPermission
                @hasPermission('category-view')
                <ul class="side-nav">
                    <li class="side-nav-item {{ request()->is('products/*') ? 'menuitem-active' : '' }}">
                        <a href="{{ route('products.index') }}" class="side-nav-link">
                            <i class="mdi mdi-slide "></i>
                            <span>Sliders</span>
                        </a>
                    </li>
                </ul>
                @endhasPermission
                @hasPermission('category-view')
                <ul class="side-nav">
                    <li class="side-nav-item {{ request()->is('products/*') ? 'menuitem-active' : '' }}">
                        <a href="{{ route('products.index') }}" class="side-nav-link">
                            <i class="mdi mdi-account-edit "></i>
                            <span>Settings</span>
                        </a>
                    </li>
                </ul>
                @endhasPermission
                <!-- End Sidebar -->

                <div class="clearfix"></div>

            </div>
            <!-- Sidebar -left -->
            <!-- Topbar Start -->
            <div class="navbar-custom">
                <ul class="list-unstyled topbar-menu float-end mb-0">
                    <li class="dropdown notification-list d-lg-none">
                        <a class="nav-link dropdown-toggle arrow-none" data-bs-toggle="dropdown" href="#" role="button"
                            aria-haspopup="false" aria-expanded="false">
                            <i class="dripicons-search noti-icon"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-animated dropdown-lg p-0">
                            <form class="p-3">
                                <input type="text" class="form-control" placeholder="Search ..."
                                    aria-label="Recipient's username">
                            </form>
                        </div>
                    </li>

                    <li class="dropdown notification-list">
                        <a class="nav-link dropdown-toggle nav-user arrow-none me-0" data-bs-toggle="dropdown" href="#"
                            role="button" aria-haspopup="false" aria-expanded="false">
                            <span class="account-user-avatar">
                                <img src="{{ asset('assets/images/users/avatar-1.jpg') }}" alt="user-image"
                                    class="rounded-circle">
                            </span>
                            <span>
                                <span class="account-user-name">{{ auth()->user()->name ?? 'Guest' }}</span>
                                <span
                                    class="account-position">{{ auth()->user()->roles()->first()->display_name ?? 'User' }}</span>
                            </span>
                        </a>
                        <div
                            class="dropdown-menu dropdown-menu-end dropdown-menu-animated topbar-dropdown-menu profile-dropdown">
                            <!-- item-->
                            <div class=" dropdown-header noti-title">
                                <h6 class="text-overflow m-0">Welcome !</h6>
                            </div>
                            <!-- item-->
                            <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <i class="mdi mdi-account-circle me-1"></i>
                                <span>My Account</span>
                            </a>
                            <!-- item-->
                            <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <i class="mdi mdi-account-edit me-1"></i>
                                <span>Settings</span>
                            </a>
                            <!-- item-->
                            <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <i class="mdi mdi-lifebuoy me-1"></i>
                                <span>Support</span>
                            </a>
                            <!-- item-->
                            <a href="javascript:void(0);" class="dropdown-item notify-item">
                                <i class="mdi mdi-lock-outline me-1"></i>
                                <span>Lock Screen</span>
                            </a>
                            <!-- item-->
                            <form action="{{ route('logout') }}" method="post" class="dropdown-item notify-item">
                                @csrf
                                <i class="mdi mdi-logout me-1"></i>
                                <button class="btn">Logout</button>
                            </form>
                        </div>
                    </li>
                </ul>
                <button class="button-menu-mobile open-left">
                    <i class="mdi mdi-menu"></i>
                </button>
            </div>
            <!-- end Topbar -->

        </div>
        <div class="content-page">
            @yield('content')
            <!-- Footer Start -->
            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <script>
                                document.write(new Date().getFullYear())
                            </script>
                            2022 © Hyper - Coderthemes.com
                        </div>
                        <div class="col-md-6">
                            <div class="text-md-end footer-links d-none d-md-block">
                                <a href="javascript: void(0);">About</a>
                                <a href="javascript: void(0);">Support</a>
                                <a href="javascript: void(0);">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- end Footer -->
        </div>
    </div>
    <!-- END wrapper -->

    <!-- Right Sidebar -->
    <div class="end-bar">

        <div class="rightbar-title">
            <a href="javascript:void(0);" class="end-bar-toggle float-end">
                <i class="dripicons-cross noti-icon"></i>
            </a>
            <h5 class="m-0">Settings</h5>
        </div>

        <div class="rightbar-content h-100" data-simplebar="">

            <div class="p-3">
                <div class="alert alert-warning" role="alert">
                    <strong>Customize </strong> the overall color scheme, sidebar menu, etc.
                </div>

                <!-- Settings -->
                <h5 class="mt-3">Color Scheme</h5>
                <hr class="mt-1">

                <div class="form-check form-switch mb-1">
                    <input class="form-check-input" type="checkbox" name="color-scheme-mode" value="light"
                        id="light-mode-check" checked="">
                    <label class="form-check-label" for="light-mode-check">Light Mode</label>
                </div>

                <div class="form-check form-switch mb-1">
                    <input class="form-check-input" type="checkbox" name="color-scheme-mode" value="dark"
                        id="dark-mode-check">
                    <label class="form-check-label" for="dark-mode-check">Dark Mode</label>
                </div>


                <!-- Width -->
                <h5 class="mt-4">Width</h5>
                <hr class="mt-1">
                <div class="form-check form-switch mb-1">
                    <input class="form-check-input" type="checkbox" name="width" value="fluid" id="fluid-check"
                        checked="">
                    <label class="form-check-label" for="fluid-check">Fluid</label>
                </div>

                <div class="form-check form-switch mb-1">
                    <input class="form-check-input" type="checkbox" name="width" value="boxed" id="boxed-check">
                    <label class="form-check-label" for="boxed-check">Boxed</label>
                </div>


                <!-- Left Sidebar-->
                <h5 class="mt-4">Left Sidebar</h5>
                <hr class="mt-1">
                <div class="form-check form-switch mb-1">
                    <input class="form-check-input" type="checkbox" name="theme" value="default" id="default-check">
                    <label class="form-check-label" for="default-check">Default</label>
                </div>

                <div class="form-check form-switch mb-1">
                    <input class="form-check-input" type="checkbox" name="theme" value="light" id="light-check"
                        checked="">
                    <label class="form-check-label" for="light-check">Light</label>
                </div>

                <div class="form-check form-switch mb-3">
                    <input class="form-check-input" type="checkbox" name="theme" value="dark" id="dark-check">
                    <label class="form-check-label" for="dark-check">Dark</label>
                </div>

                <div class="form-check form-switch mb-1">
                    <input class="form-check-input" type="checkbox" name="compact" value="fixed" id="fixed-check"
                        checked="">
                    <label class="form-check-label" for="fixed-check">Fixed</label>
                </div>

                <div class="form-check form-switch mb-1">
                    <input class="form-check-input" type="checkbox" name="compact" value="condensed"
                        id="condensed-check">
                    <label class="form-check-label" for="condensed-check">Condensed</label>
                </div>

                <div class="form-check form-switch mb-1">
                    <input class="form-check-input" type="checkbox" name="compact" value="scrollable"
                        id="scrollable-check">
                    <label class="form-check-label" for="scrollable-check">Scrollable</label>
                </div>

                <div class="d-grid mt-4">
                    <button class="btn btn-primary" id="resetBtn">Reset to Default</button>

                    <a href="../../product/hyper-responsive-admin-dashboard-template/index.htm"
                        class="btn btn-danger mt-3" target="_blank"><i class="mdi mdi-basket me-1"></i> Purchase
                        Now</a>
                </div>
            </div> <!-- end padding-->

        </div>
    </div>

    <div class="rightbar-overlay"></div>
    <!-- /End-bar -->

    <!-- bundle -->
    <script src="{{ asset('assets/js/vendor.min.js') }}" defer></script>
    <script src="{{ asset('assets/js/app.min.js') }}" defer></script>

    <!-- third party js -->
    <script src="{{ asset('assets/js/vendor/apexcharts.min.js') }}" defer></script>
    <script src="{{ asset('assets/js/vendor/jquery-jvectormap-1.2.2.min.js') }}" defer></script>
    <script src="{{ asset('assets/js/vendor/jquery-jvectormap-world-mill-en.js') }}" defer></script>
    <!-- third party js ends -->

    <!-- demo app -->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11" defer></script>
    <script src="{{ asset('assets/js/pages/demo.dashboard.js') }}" defer></script>
    <script src="{{ asset('admin/js/base.js') }}" defer></script>
    @stack('js')
    <!-- end demo js-->
</body>

</html>
