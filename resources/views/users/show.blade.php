@extends('layouts.app')
@section('content')
    <div class="content-page mx-2">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title">User Id: {{ $user->id }}</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="mb-3">
                            <label for="projectname" class="form-label">Name</label>
                            <input disabled type="text" name="name" class="form-control" value="{{ $user->name }}"
                                   placeholder="Enter Your User Name">
                        </div>
                        <!-- Date View -->
                        <div class="mb-3">
                            <label for="project-budget" class="form-label">Email</label>
                            <input disabled type="text" name="email" class="form-control"
                                   placeholder="Enter Your Email" value="{{ $user->email }}">
                        </div>
                        <div class="mb-3 position-relative" id="datepicker1">
                            <label class="form-label">Birthday</label>
                            <input disabled type="text" name="birthday" class="form-control" data-provide="datepicker"
                                   data-date-container="#datepicker1" placeholder="Chose Your Birthday"
                                   value="{{ $user->birthday }}">
                        </div>
                        <div class="mb-3">
                            <label for="project-budget" class="form-label">Address</label>
                            <input disabled type="text" name="address" class="form-control"
                                   placeholder="Enter Your Address" value="{{ $user->address }}">
                        </div>
                        <div class="mb-3">
                            <label for="project-budget" class="form-label">Phone Number</label>
                            <input disabled type="text" name="phone_number" class="form-control"
                                   placeholder="Enter Your Number" value="{{ $user->phone_number }}">
                        </div>
                        <div class="mb-3">
                            <label for="example-select" class="form-label">Gender</label>
                            <select disabled class="form-select" id="example-select" name="gender">
                                <option value="0" @selected($user->gender == 0)>Male</option>
                                <option value="1" @selected($user->gender == 1)>Female</option>
                            </select>
                        </div>
                        <!-- Preview -->
                        <div class="mb-3">
                            <label for="project-budget" class="form-label">Avatar</label>
                        </div>
                        <img src="{{ $user->image  }}" class="rounded bg-light image-data img-thumbnail .img-fluid"
                             style="max-width: 30%; height: auto;"/>
                    </div>
                    <div class="form-check mt-3">
                        <input disabled class="form-check-input check-all-permission select-all" type="checkbox"
                               id="checkAll">
                        <label class="form-label" for="checkAll">ALL</label>
                        <div class="row">
                            @foreach($roles as $role)
                                <div class="card-text">
                                    <div class="form-check">
                                        <input disabled
                                               class="select-item"
                                               type="checkbox" id="{{ $role->id }}"
                                               name="role_ids[]"
                                               value="{{ $role->id }}"
                                            {{ $user->roles->contains($role->id) ? 'checked' : '' }}
                                        >
                                        <label class="form-check-label"
                                               for="{{ $role->id }}">
                                            {{ $role->display_name }}
                                        </label>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div> <!-- end col-->
            </div>
        </div>
    </div>
    </div>
@endsection
@push('js')
    <script src="{{ asset('admin/js/user.js') }}" defer></script>
    <script src="{{ asset('admin/js/handleUploadImage.js') }}" defer></script>

@endpush
