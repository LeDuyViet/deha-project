@extends('layouts.app')

@section('content')
    @if(Session::has('message'))
        <p class="alert alert-info">{{ Session('message') }}</p>
    @endif
    <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('users.index') }}">User</a></li>
                                <li class="breadcrumb-item active">User List</li>
                            </ol>
                        </div>
                        <h4 class="page-title">User</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            @hasPermission('user-create')
            <div class="row mb-2">
                <div class="col-sm-2">
                    <a href="{{ route('users.create') }}" class="btn btn-success btn-rounded mb-3"><i
                            class="mdi mdi-plus"></i>Create</a>
                </div>
                <div class=" col-sm-8 app-search dropdown d-none d-lg-block">
                    <form method="GET">
                        <div class="d-flex">
                            <input value="{{ request()->name }}" name="name" type="text"
                                   class="form-control mx-1" placeholder="Name..." id="top-search" autocomplete="off"
                                   style="padding-left: 10px; background-color: #fff; border: 1px solid #e7ebf0;">
                            <input value="{{ request()->email }}" name="email" type="text"
                                   class="form-control mx-1" placeholder="Email..." id="top-search"
                                   style="padding-left: 10px; background-color: #fff; border: 1px solid #e7ebf0;">
                            <select class="form-control mx-1 select2" data-toggle="select2" name="role_id">
                                <option value="">Select</option>
                                <optgroup>
                                    @foreach($roles as $role)
                                        <option
                                            {{ request()->role_id == $role->id ? 'selected' : '' }} value="{{ $role->id }}">{{ $role->display_name }}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                            <button class="input-group-text btn-primary" type="submit">Search</button>
                        </div>
                    </form>

                </div>
                <div class="col-sm-2">
                    <form class="w-50 float-end" method="GET">
                        <select class="form-control mx-1 select2 select-paginate" data-toggle="select2" name="paginate_number">
                            <option value="">Select</option>
                            <optgroup>
                                @for($i = 5; $i <= 50; $i+=5)
                                    <option
                                        {{ request()->paginate_number == $i ? 'selected' : '' }} value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </optgroup>
                        </select>
                        <button class="input-group-text btn-primary btn-change-paginate d-none" type="submit">Submit</button>
                    </form>
                </div>
            </div>
            @endhasPermission
            <!-- start row-->
            <div class="row">
                <div class="col-sm-12">
                    <table
                        class="table table-centered table-striped dt-responsive nowrap w-100 dataTable no-footer dtr-inline"
                        id="products-datatable" role="grid" aria-describedby="products-datatable_info"
                        style="width: 1554px;">
                        <thead>
                        <tr role="row">
                            <th tabindex="0" aria-controls="products-datatable" rowspan="1" colspan="1"
                                aria-label="Customer: activate to sort column ascending" style="min-width: 200px;">
                                {{ucfirst('name')}}
                            </th>
                            <th tabindex="0" aria-controls="products-datatable" rowspan="1" colspan="1"

                                aria-label="Phone: activate to sort column ascending">{{ucfirst('email')}}
                            </th>
                            <th tabindex="0" aria-controls="products-datatable" rowspan="1" colspan="1"
                                aria-label="Status: activate to sort column ascending">
                                {{ucfirst('phone number')}}
                            </th>
                            <th class="sorting_disabled" rowspan="1" colspan="1"
                                aria-label="Action"> {{ucfirst('Roles')}}
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        @forelse($users as $user)
                                <td class="table-user">
                                    <img src="{{ $user->image }}" alt="table-user"
                                         class="me-2 rounded-circle">
                                    <a href="{{ route('users.show', $user->id) }}" class="text-body fw-semibold">{{ $user->name }}</a>
                                </td>
                                <td>
                                    {{ $user->email }}
                                </td>
                                <td class="sorting_1">
                                    {{ $user->phone_number }}
                                </td>
                                <td>
{{--                                    {{ $user->gender }}--}}
                                    @foreach($user->roles()->get() as $role)
{{--                                        <option {{ request()->role_id == $role->id ? 'selected' : '' }} value="{{ $role->id }}"></option>--}}
                                        <span class="badge bg-success">{{ $role->display_name }}</span>
                                    @endforeach
                                </td>
                                <td class="d-flex justify-content-sm-evenly align-items-center">
                                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-warning"> <i
                                            class="mdi mdi-square-edit-outline"></i></a>
                                    @hideMyAccount($user->id)
                                    <form action="{{ route('users.delete', $user->id) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-danger"><i
                                                class="mdi mdi-delete"></i></button>
                                    </form>
                                    @endhideMyAccount
                                </td>
                        </tr>
                        @empty
                            <tbody class="">
                            <td colspan="8" class="text-center"><h1 class="">No matching categories found</h1></td>
                            </tbody>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- end row-->
            {{--                        {{ $users->links() }}--}}
            {{ $users->appends(request()->all())->links() }}
        </div> <!-- container -->
    </div>
@endsection
@push('js')
    <script src="{{ asset('admin/js/user.js')}}" defer></script>
@endpush
