@extends('layouts.app')
@section('content')
    <div class="content-page mx-2">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title">Create</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <form action="{{ route('users.store') }}" method="post" enctype="multipart/form-data">
                        <div class="col-xl-12">
                            @csrf
                            <div class="mb-3">
                                <label for="projectname" class="form-label">Name</label>
                                <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Enter Your User Name">
                            </div>
                            @error('name')
                            <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                 user="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                <strong>Error - </strong> {{ $message }}
                            </div>
                            @enderror
                        <!-- Date View -->
                            <div class="mb-3">
                                <label for="project-budget" class="form-label">Email</label>
                                <input type="text" name="email" class="form-control"
                                       placeholder="Enter Your Email" value="{{ old('email') }}" >
                            </div>
                            @error('email')
                            <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                 user="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                <strong>Error - </strong> {{ $message }}
                            </div>
                            @enderror
                            <div class="mb-3 position-relative" id="datepicker1">
                                <label class="form-label">Birthday</label>
                                <input type="text" name="birthday" class="form-control" data-provide="datepicker" data-date-container="#datepicker1" placeholder="Chose Your Birthday" value="{{ old('birthday') }}">
                            </div>
                            @error('birthday')
                            <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                 user="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                <strong>Error - </strong> {{ $message }}
                            </div>
                            @enderror
                            <div class="mb-3">
                                <label for="project-budget" class="form-label">Address</label>
                                <input type="text" name="address" class="form-control"
                                       placeholder="Enter Your Address" value="{{ old('address') }}">
                            </div>
                            @error('address')
                            <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                 user="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                <strong>Error - </strong> {{ $message }}
                            </div>
                            @enderror
                            <div class="mb-3">
                                <label for="project-budget" class="form-label">Phone Number</label>
                                <input type="text" name="phone_number" class="form-control"
                                       placeholder="Enter Your Number" value="{{ old('phone_number') }}">
                            </div>
                            @error('phone_number')
                            <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                 user="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                <strong>Error - </strong> {{ $message }}
                            </div>
                            @enderror
                            <div class="mb-3">
                                <label for="example-select" class="form-label">Gender</label>
                                <select class="form-select" id="example-select" name="gender">
                                    <option value="0" @selected(old('gender') == 0)>Male</option>
                                    <option value="1" @selected(old('gender') == 1)>Female</option>
                                </select>
                            </div>
                            @error('gender')
                            <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                 user="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                <strong>Error - </strong> {{ $message }}
                            </div>
                            @enderror
                            <div class="mb-3">
                                <label for="example-select" class="form-label">Password</label>
                                <input type="password" name="password" class="form-control"
                                       placeholder="Enter Your Password">
                            </div>
                            @error('password')
                            <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                 user="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                <strong>Error - </strong> {{ $message }}
                            </div>
                            @enderror
                            <div class="mb-3">
                                <label for="example-fileinput" class="form-label">Image</label>
                                <input type="file" name="image" class="form-control image-input">
                            </div>
                            @error('image')
                            <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                 user="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                <strong>Error - </strong> {{ $message }}
                            </div>
                        @enderror
                        <!-- Preview -->
                            <div class="dropzone-previews mt-3 position-relative" id="file-previews"
                                 style="display: none">
                                <button type="button" class="btn-close position-absolute close-image" style="top: 2%; left: 0.5%;"
                                        aria-label="Close"></button>
                                <img class="rounded bg-light image-data img-thumbnail .img-fluid"
                                     style="max-width: 30%; height: auto;"/>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input check-all-permission select-all" type="checkbox"
                                       id="checkAll">
                                <label class="form-label" for="checkAll">ALL</label>
                                <div class="row">
                                    @foreach($roles as $role)
                                        <div class="card-text">
                                            <div class="form-check">
                                                <input
                                                    class="select-item"
                                                    type="checkbox" id="{{ $role->id }}"
                                                    name="role_ids[]"
                                                    value="{{ $role->id }}"
                                                    {{ is_array(old('role_ids')) && in_array($role->id, old('role_ids')) ? 'checked' : ''}}
                                                >
                                                <label class="form-check-label"
                                                       for="{{ $role->id }}">
                                                    {{ $role->display_name }}
                                                </label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div> <!-- end col-->
                        <div class="text-center mt-3">
                            <button class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script src="{{ asset('admin/js/user.js') }}" defer></script>
    <script src="{{ asset('admin/js/handleUploadImage.js') }}" defer></script>

@endpush
