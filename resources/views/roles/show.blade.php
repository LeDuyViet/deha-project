@extends('layouts.app')
@section('content')
    <div class="content-page mx-2">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title">Show Role</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                            <div class="mb-3">
                                <label for="projectname" class="form-label">Name</label>
                                <input disabled type="text" name="name" class="form-control" placeholder="Enter role name" value="{{ $role->name }}">
                            </div>

                        <!-- Date View -->
                            <div class="mb-3">
                                <label for="project-budget" class="form-label">Display Name</label>
                                <input disabled type="text" name="display_name" class="form-control" placeholder="Enter display name" value="{{ $role->display_name }}">
                            </div>
                            <div class="mb-3">
                                <label for="example-fileinput" class="form-label">Image</label><br>
                                <img class="img-fluid" src="{{ $role->image ? asset('/uploads/' . $role->image) : asset('/uploads/default.jpg')}}">
                            </div>
                            <div class="form-check">
                                <input disabled class="form-check-input check-all-permission select-all" type="checkbox"
                                       id="checkAll">
                                <label class="form-label" for="checkAll">ALL</label>
                                <div class="row">
                                    @foreach($permissionGroups as $group => $permissions)
                                        <div class="col checkbox-wrapper">
                                            <input disabled
                                                class="select-group"
                                                type="checkbox" id="{{ $group }}">
                                            <label class="form-label"
                                                   for="{{ $group }}"><b>{{ ucfirst( $group )}}</b></label>
                                            @foreach($permissions as $permission)
                                                <div class="card-text">
                                                    <div class="form-check">
                                                        <input disabled
                                                            {{ $role->permissions->contains($permission->id) ? 'checked' : '' }}
                                                            class="select-item"
                                                            type="checkbox" id="{{ $permission->id }} "
                                                            name="permission_names[]"
                                                            value="{{ $permission->id }}">
                                                        <label class="form-check-label"
                                                               for="{{ $permission->id }}">
                                                            {{ $permission->display_name }}
                                                        </label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                    </div> <!-- end col-->
                </div>
            </div>
        </div>
    </div>
    @endsection

@push('js')
    <script src="{{ asset('admin/js/role.js') }}" defer></script>
@endpush
