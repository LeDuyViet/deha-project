@extends('layouts.app')

@section('content')
    @if (Session::has('message'))
        <p class="alert alert-info">{{ Session('message') }}</p>
    @endif
    <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('roles.index') }}">Role</a></li>
                                <li class="breadcrumb-item active">Role List</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Role</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            <div class="row mb-2">
                <div class="col-sm-4">
                    @hasPermission('role-create')
                    <a href="{{ route('roles.create') }}" class="btn btn-success btn-rounded mb-3"><i
                            class="mdi mdi-plus"></i>Create</a>
                    @endhasPermission
                </div>

                <div class=" col-sm-4 app-search dropdown d-none d-lg-block">
                    <form method="GET">
                        <div class="input-group">
                            <input value="{{ request()->name }}" name="name" type="text"
                                class="form-control dropdown-toggle" placeholder="Search..." id="top-search">
                            <span class="mdi mdi-magnify search-icon"></span>
                            <button class="input-group-text btn-primary" type="submit">Search</button>
                        </div>
                    </form>
                </div>
                <div class="col-sm-4">
                    <form class="w-25 float-end" method="GET">
                        <select class="form-control mx-1 select2 select-paginate" data-toggle="select2" name="paginate_number">
                            <option value="">Select</option>
                            <optgroup>
                                @for($i = 4; $i <= 50; $i+=4)
                                    <option
                                        {{ request()->paginate_number == $i ? 'selected' : '' }} value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </optgroup>
                        </select>
                        <button class="input-group-text btn-primary btn-change-paginate d-none" type="submit">Submit</button>
                    </form>
                </div>
            </div>
            <!-- start row-->
            <div class="row">
                @forelse ($roles as $role)
                    <div class="col-md-6 col-xxl-3">
                        <!-- project card -->
                        <div class="card d-block">
                            <!-- project-thumbnail -->
                            <img class="card-img-top" src="{{ $role->image ?? asset('uploads/' . 'default.jpg') }}"
                                alt="project image cap">
                            <div class="card-body position-relative">
                                <div class="progress progress-sm">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0"
                                        aria-valuemax="100"
                                        style="width: {{ $role->name == 'super-admin' ? '100' : ($role->permissions_count / $permissionCount) * 100 }}%;">
                                    </div>
                                </div>
                                <h4 class="mt-2">
                                    <a href="{{ route('roles.show', $role->id) }}"
                                        class="text-title">{{ $role->display_name }}</a>
                                </h4>
                                <!-- project detail-->
                                <p class="mb-3">
                                    <span class="pe-2 text-nowrap">
                                        <i class="mdi mdi-format-list-bulleted-type"></i>
                                        <b>{{ $role->name == 'super-admin' ? 'Full' : $role->permissions_count }}</b>
                                        Permission
                                    </span>
                                    <span class="text-nowrap">
                                        <i class="mdi mdi-comment-multiple-outline"></i>
                                        <b>{{ $role->users_count }} People</b>
                                    </span>
                                </p>
                                <div class="justify-content-center d-flex mt-3">
                                    @hasPermission('role-edit')
                                    <form action="{{ route('roles.edit', $role->id) }}" class="mx-1">
                                        <button type="submit" class="btn btn-warning"><i class="mdi mdi-wrench"></i>
                                        </button>
                                    </form>
                                    @endhasPermission
                                    @hideInMyRole($role->name)
                                    @hasPermission('role-delete')
                                    <form action="{{ route('roles.delete', $role->id) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger"><i class="mdi mdi-window-close"></i>
                                        </button>
                                    </form>
                                    @endhasPermission
                                    @endhideInMyRole
                                </div>
                            </div> <!-- end card-body-->
                        </div> <!-- end card-->
                    </div> <!-- end col -->
                @empty
                    <div class="mt-5 d-flex justify-content-center align-items-center">
                        <h1 colspan="8" class="dataTables_empty">No matching roles found</h1>
                    </div>
                @endforelse
            </div>
            <!-- end row-->
            {{-- {{ $roles->links() }} --}}
            {{ $roles->appends(request()->all())->links() }}
        </div> <!-- container -->
    </div>
@endsection
@push('js')
    <script src="{{ asset('admin/js/role.js') }}" defer></script>
@endpush
