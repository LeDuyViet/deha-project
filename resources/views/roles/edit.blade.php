@extends('layouts.app')
@section('content')
    <div class="content-page mx-2">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title">Edit Role</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <form action="{{ route('roles.update', $role) }}" method="post"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="mb-3">
                                <label for="projectname" class="form-label">Name</label>
                                <input type="text" name="name" class="form-control" placeholder="Enter role name"
                                       value="{{ $role->name }}">
                            </div>
                            @error('name')
                            <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                 role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                <strong>Error - </strong> {{ $message }}
                            </div>
                            @enderror
                        <!-- Date View -->
                            <div class="mb-3">
                                <label for="project-budget" class="form-label">Display Name</label>
                                <input type="text" name="display_name" class="form-control"
                                       placeholder="Enter display name" value="{{ $role->display_name }}">
                            </div>
                            @error('display_name')
                            <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                 role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                <strong>Error - </strong> {{ $message }}
                            </div>
                            @enderror
                            <div class="mb-3">
                                <label for="example-fileinput" class="form-label">Image</label>
                                <input type="file" name="image" class="form-control image-input" alt="{{ $role->image }}">
                            </div>
                            @error('image')
                            <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                 role="alert">
                                <button type="button" class="btn-close " data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                <strong>Error - </strong> {{ $message }}
                            </div>
                            @enderror
                            <!-- Preview -->
                            <div class="dropzone-previews mt-3 position-relative" id="file-previews">
                                <button type="button" class="btn-close position-absolute close-image" style="top: 3%; left: 0.5%;" aria-label="Close"></button>
                                <img src="{{ $role->image }}" class="rounded bg-light image-data img-thumbnail" style="max-width: 30%; height: auto;"/>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input check-all-permission select-all" type="checkbox"
                                       id="checkAll">
                                <label class="form-label" for="checkAll">ALL</label>
                                <div class="row">
                                    @foreach($permissionGroups as $group => $permissions)
                                        <div class="col checkbox-wrapper">
                                            <input
                                                class="select-group"
                                                type="checkbox" id="{{ $group }}">
                                            <label class="form-label"
                                                   for="{{ $group }}"><b>{{ ucfirst( $group )}}</b></label>
                                            @foreach($permissions as $permission)
                                                <div class="card-text">
                                                    <div class="form-check">
                                                        <input
                                                            {{ $role->permissions->contains($permission->id) ? 'checked' : '' }}
                                                            class="select-item"
                                                            type="checkbox" id="{{ $permission->id }} "
                                                            name="permission_names[]"
                                                            value="{{ $permission->id }}">
                                                        <label class="form-check-label"
                                                               for="{{ $permission->id }}">
                                                            {{ $permission->display_name }}
                                                        </label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div> <!-- end col-->
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('admin/js/role.js') }}" defer></script>
    <script src="{{ asset('admin/js/handleUploadImage.js') }}" defer></script>
@endpush
