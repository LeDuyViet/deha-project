@extends('layouts.app')
@section('content')
    <div class="content-page mx-2">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title">Create Category</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <form action="{{ route('categories.store') }}" method="post" enctype="multipart/form-data">
                        <div class="col-xl-12">
                            @csrf
                            <div class="mb-3">
                                <label for="projectname" class="form-label">Name</label>
                                <input type="text" name="name" class="form-control" placeholder="Enter Category Name" value="{{ old('name') }}"}}>
                            </div>
                            @error('name')
                            <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                 role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                <strong>Error - </strong> {{ $message }}
                            </div>
                            @enderror
                            <div class="mb-3">
                                <label for="projectname" class="form-label">Parent Category</label>
                                <select class="form-control select2" data-toggle="select2" name="parent_id">
                                    <option value="">Select</option>
                                    <optgroup>
                                        @foreach($parentCategories as $category)
                                            <option @selected(old('parent_id') == $category->id) value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                            @error('parent_id')
                            <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                 category="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                <strong>Error - </strong> {{ $message }}
                            </div>
                            @enderror
                        <!-- Date View -->
                        <div class="text-center mt-3">
                            <button class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script src="{{ asset('admin/js/category.js') }}" defer></script>
    <script src="{{ asset('admin/js/handleUploadImage.js') }}" defer></script>

@endpush
