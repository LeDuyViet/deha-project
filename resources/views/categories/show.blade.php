@extends('layouts.app')
@section('content')
    <div class="content-page mx-2">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box">
                            <h4 class="page-title">Show</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="mb-3">
                            <label for="projectname" class="form-label">Name</label>
                            <input disabled type="text" name="name" class="form-control"
                                   placeholder="Enter Category Name"
                                   value="{{ $category->name }}">
                        </div>
                        @if(!$category->childrens()->exists())
                            <div class="mb-3">
                                <label for="projectname" class="form-label">Parent Category</label>
                                <input disabled type="text" name="name" class="form-control"
                                       placeholder="Enter Category Name"
                                       value="{{ $category->parents->name ?? "" }}">
                            </div>
                        @endif
                    </div>
                </div>
                @if($category->childrens()->exists())
                    <div class="row">
                        <div class="col-sm-12">
                            <table
                                class="table table-centered table-striped dt-responsive nowrap w-100 dataTable no-footer dtr-inline"
                                id="products-datatable" role="grid" aria-describedby="products-datatable_info"
                                style="width: 1554px;">
                                <thead>
                                <tr role="row">
                                    <th tabindex="0" aria-controls="products-datatable" rowspan="1" colspan="1"
                                        aria-label="Customer: activate to sort column ascending">
                                        Childrens Categories
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    @foreach($childrens as $child)
                                        <td class="table-category">
                                            <a href="{{ route('categories.show', $child->id) }}"
                                               class="text-body fw-semibold">{{ $child->name }}</a>
                                        </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- end row-->
                    {{ $childrens->links() }}
{{--                                    {{ $category->childrens()->appends(request()->all())->links() }}--}}
            </div> <!-- container -->
        </div>
    </div>
    @endif
@endsection
@push('js')
    <script src="{{ asset('admin/js/category.js') }}" defer></script>
    <script src="{{ asset('admin/js/handleUploadImage.js') }}" defer></script>

@endpush
