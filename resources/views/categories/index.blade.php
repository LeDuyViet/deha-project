@extends('layouts.app')

@section('content')
    @if(Session::has('message'))
        <p class="alert alert-info">{{ Session('message') }}</p>
    @endif
    <div class="content">
        <!-- Start Content-->
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('categories.index') }}">Category</a></li>
                                <li class="breadcrumb-item active">Category List</li>
                            </ol>
                        </div>
                        <h4 class="page-title">Category</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->
            @hasPermission('category-create')
            <div class="row mb-2">
                <div class="col-sm-2">
                    <a href="{{ route('categories.create') }}" class="btn btn-success btn-rounded mb-3"><i
                            class="mdi mdi-plus"></i>Create</a>
                </div>
                <div class=" col-sm-8 app-search dropdown d-none d-lg-block">
                    <form method="GET">
                        <div class="d-flex">
                            <input value="{{ request()->name }}" name="name" type="text"
                                   class="form-control mx-1 dropdown-toggle" placeholder="Name..." id="top-search" autocomplete="off" style="padding-left: 10px; background-color: #fff; border: 1px solid #e7ebf0;">
                            <select class="form-control mx-1 select2" data-toggle="select2" name="parent_id">
                                <option value="">Select</option>
                                <optgroup>
                                    @foreach($parentCategories as $parentCategory)
                                        <option {{ request()->parent_id == $parentCategory->id ? 'selected' : '' }} value="{{ $parentCategory->id }}">{{ $parentCategory->name }}</option>
                                    @endforeach
                                </optgroup>
                            </select>
                            <button class="input-group-text btn-primary" type="submit">Search</button>
                        </div>
                    </form>

                </div>
            </div>
            @endhasPermission
            <!-- start row-->
            <div class="container" style="margin-top:30px;">
                <div class="row">
                    @forelse($categories as $category)
                        <ul id="tree2" class="col-sm-2 card mx-1 p-2">
                            <li class="list-unstyled"><a href="{{ route('categories.edit', $category->id) }}" class=""><i class="uil-book"></i>{{ $category->name }}</a>
                                @foreach($category->childrens()->get() as $child)
                                <ul>
                                    <li><a class="text-info" href="{{ route('categories.edit', $child->id) }}">{{ $child->name }}</a></li>
                                </ul>
                                @endforeach
                            </li>
                        </ul>
                    @empty
                        <tbody class="">
                        <td colspan="8"><h1 class="">No matching categories found</h1></td>
                        </tbody>
                    @endforelse
                </div>
            </div>
        </div>
        {{ $categories->appends(request()->all())->links() }}
    </div> <!-- container -->
@endsection
@push('js')
    <script src="{{ asset('admin/js/category.js')}}" defer></script>
@endpush
