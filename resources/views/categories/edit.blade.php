@extends('layouts.app')
@section('content')
    <div class="content-page mx-2">
        <div class="content">
            <div class="container-fluid">
                <div class="row d-flex align-items-center">
                    <div class="col-6">
                        <div class="page-title-box">
                            <h4 class="page-title">Edit</h4>
                        </div>
                    </div>
                    <div class="col-6 text-end">
                            <form action="{{ route('categories.delete', $category->id) }}" method="post">
                                @csrf
                                @method('delete')
                                <button class="mt-1 btn btn-danger"><i
                                        class="mdi mdi-delete"></i></button>
                            </form>
                    </div>
                </div>
                <div class="row">
                    <form action="{{ route('categories.update', $category) }}" method="post" enctype="multipart/form-data">
                        <div class="col-xl-12">
                            @csrf
                            @method('put')
                            <div class="mb-3">
                                <label for="projectname" class="form-label">Name</label>
                                <input type="text" name="name" class="form-control" placeholder="Enter Category Name" value="{{ $category->name }}">
                            </div>
                            @error('name')
                            <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                 role="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                <strong>Error - </strong> {{ $message }}
                            </div>
                            @enderror
                            @if(!$category->childrens()->exists())
                            <div class="mb-3">
                                <label for="projectname" class="form-label">Parent Category</label>
                                <select class="form-control select2" data-toggle="select2" name="parent_id">
                                    <option value="">Select</option>
                                    <optgroup>
                                        @foreach($parentCategories as $parentCategory)
                                            <option {{ $parentCategory->id == $category->parent_id ? 'selected' : ''}} value="{{ $parentCategory->id }}">{{ $parentCategory->name }}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                            </div>
                            @endif
                            @error('parent_id')
                            <div class="alert alert-danger alert-dismissible bg-danger text-white border-0 fade show"
                                 category="alert">
                                <button type="button" class="btn-close" data-bs-dismiss="alert"
                                        aria-label="Close"></button>
                                <strong>Error - </strong> {{ $message }}
                            </div>
                            @enderror
                        <!-- Date View -->
                        <div class="text-center mt-3">
                            <button class="btn btn-primary">Update</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script src="{{ asset('admin/js/category.js') }}" defer></script>
    <script src="{{ asset('admin/js/handleUploadImage.js') }}" defer></script>

@endpush
