const users = (function () {
    modules = {}

    modules.selectAllItem = function (selectAll, selectItem) {
        $(`.${selectAll}`).on('change', function () {
            $(`.${selectItem}`).prop('checked', $(this).prop("checked"));
        });
    }

    modules.selectItem = function (selectAll, selectItem) {
        $(`.${selectItem}`).on('change', function () {
            // select all
            if ($(`.${selectItem}:checked`).length === $(`.${selectItem}`).length) {
                $(`.${selectAll}`).prop('checked', true)
            } else {
                $(`.${selectAll}`).prop('checked', false)
            }
        })
    }

    modules.deleteUser = function () {
        $('.btn.btn-danger').on('click', function (event) {
            event.preventDefault();
            CustomAlert.confirmButton('Are you sure you want to delete', 'Delete successfully', 'Delete cancelled')
                .then(function (result) {
                        CustomAlert.alertSucces('Delete user successfully')
                        $(event.target).closest('form').submit()
                })
        });
    }

    return modules
})()

const selectAll = 'select-all'
const selectItem = 'select-item'

users.selectAllItem(selectAll, selectItem)
users.selectItem(selectAll, selectItem)
users.deleteUser()

