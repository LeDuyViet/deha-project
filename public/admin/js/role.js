const roles = (function () {
    modules = {}

    modules.selectAllItem = function (selectAll, selectGroup, selectItem) {
        $(`.${selectAll}`).on('change', function () {
            $(`.${selectItem}`).prop('checked', $(this).prop("checked"));
            $(`.${selectGroup}`).prop('checked', $(this).prop("checked"));
        });
    }

    modules.selectGroup = function (selectAll, selectGroup, selectItem) {
        $(`.${selectGroup}`).on('change', function () {
            $(this).parents('.checkbox-wrapper').find(`.${selectItem}`).prop('checked', $(this).prop('checked'))
            if ($(`.${selectGroup}:checked`).length === $(`.${selectGroup}`).length) {
                $(`.${selectAll}`).prop('checked', true)
            } else {
                $(`.${selectAll}`).prop('checked', false)
            }
        })
    }

    modules.selectItem = function (selectAll, selectGroup, selectItem) {
        $(`.${selectItem}`).on('change', function () {
            // select all
            if ($(`.${selectItem}:checked`).length === $('.selectItem').length) {
                $(`.${selectAll}`).prop('checked', true)
            } else {
                $(`.${selectAll}`).prop('checked', false)
            }
            // select group
            if ($(this).parents('.checkbox-wrapper').find(`.${selectItem}`).length === $(this).parents('.checkbox-wrapper').find(`.${selectItem}:checked`).length) {
                $(this).parents('.checkbox-wrapper').find(`.${selectGroup}`).prop('checked', true);
            } else {
                $(this).parents('.checkbox-wrapper').find(`.${selectGroup}`).prop('checked', false);
            }
            // select all
            if ($(`.${selectItem}:checked`).length === $(`.${selectItem}`).length) {
                $(`.${selectAll}`).prop('checked', true)
            } else {
                $(`.${selectAll}`).prop('checked', false)
            }
        })
    }

    modules.deleteRole = function () {
        $('.btn.btn-danger').on('click', function (event) {
            event.preventDefault();
            CustomAlert.confirmButton('Are you sure you want to delete')
                .then(function (result) {
                    CustomAlert.alertSucces('Delete role successfully')
                    $(event.target).closest('form').submit()
                })
        });
    }

    return modules
})()

const selectAll = 'select-all'
const selectGroup = 'select-group'
const selectItem = 'select-item'

roles.selectAllItem(selectAll, selectGroup, selectItem)
roles.selectGroup(selectAll, selectGroup, selectItem)
roles.selectItem(selectAll, selectGroup, selectItem)
roles.deleteRole()

