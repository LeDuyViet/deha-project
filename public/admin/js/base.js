$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

const CustomAlert = (function () {
    let modules = {};

    modules.alertSucces = function (message = '') {
        Swal.fire({
            icon: 'success',
            title: message,
            showConfirmButton: true,
            timer: 500
        })
    }

    modules.alertError = function (message) {
        Swal.fire({
            icon: 'error',
            title: message,
            showConfirmButton: true,
            timer: 1000
        })
    }

    modules.confirmButton = function (messageConfirm) {
        return new Promise((resolve, reject) => {
            Swal.fire({
                title: 'Are you sure?',
                text: messageConfirm,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                cancelButtonText: 'No',
                reverseButtons: true
            }).then(function (result) {
                if (result.isConfirmed) {
                        resolve(true)
                } else {
                    reject(false)
                }
            });
        })
    }

    return modules
}())

const Base = (function () {
    let modules = {};

    modules.callApiNormally = function (url, data, method = 'GET') {
        return $.ajax({
            url: url,
            data: data,
            method: method
        })
    }

    modules.callApiWithFormData = function (url, data, method = 'POST') {
        return $.ajax({
            url: url,
            data: data,
            method: method,
            contentType: false,
            processData: false
        })
    }

    modules.changePaginateNumber = function () {
        $(document).on('change', '.select-paginate', function () {
            console.log('submit')
            $('.btn-change-paginate').click()
        })
    }

    return modules
})()

function debounce(func, timeout = 300) {
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            func.apply(this, args);
        }, timeout);
    };
}

Base.changePaginateNumber()
