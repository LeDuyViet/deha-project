const handleUploadImage = (function () {
    let modules = {};

    modules.selectImage = function () {
        $('.image-input').on('change', function () {
            $('.dropzone-previews').show()
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.image-data').attr('src', e.target.result)
            }
            reader.readAsDataURL(this.files[0])
        })
    }

    modules.deleteImage = function () {
        $('.btn-close.close-image').on('click', function () {
            $('.image-input').val('')
            $('.image-data').attr('src', null)
            $('.dropzone-previews').hide()
        })
    }

    return modules
})()
handleUploadImage.selectImage()
handleUploadImage.deleteImage()
