const categories = (function () {
    modules = {}

    modules.deleteCategory = function () {
        $('.btn.btn-danger').on('click', function (event) {
            event.preventDefault();
            CustomAlert.confirmButton('Are you sure you want to delete', 'Delete successfully', 'Delete cancelled')
                .then(function (result) {
                        CustomAlert.alertSucces('Delete category successfully')
                        $(event.target).closest('form').submit()
                })
        });
    }

    return modules
})()

categories.deleteCategory()

