const Products = (function () {
    let modules = {};

    modules.getList = function (url, data) {
        url = $('#list').data('action');
        data = $('.form-search').serialize();
        let page = $('#list > nav > ul > li.page-item.active > span').text();
        data = data + `&page=${page}`;
        Base.callApiNormally(url, data)
            .then(function (result) {
                $('#list').html('').append(result);
            });
    };

    modules.getListByPageLink = function (e) {
        let url = e.attr('href');
        Base.callApiNormally(url)
            .then(function (result) {
                $('#list').html('').append(result);
            });
    };

    modules.showError = function (errors) {
        $('.errors').hide();
        $('.errors').html('');
        if (errors) {
            Products.focusInputError(Object.keys(errors)[0]);
            for (const name in errors) {
                $(`.${name}_error`).html('').append(errors[name][0]);
                $(`.${name}_error`).show();
            }
        }
        Products.changeStatusForm(false);
    };

    modules.resetForm = function () {
        $('#form-create-product,#form-edit-product').trigger('reset');
        Products.closeImage();
    };

    modules.storeProduct = function (e) {
        let url = $('#form-create-product').data('action');
        let data = new FormData(document.getElementById('form-create-product'));
        Base.callApiWithFormData(url, data, 'POST')
            .then(function (result) {
                CustomAlert.alertSucces(result.message);
                $('#modal-create-product').modal('hide');
                Products.getList();
            })
            .catch(function (response) {
                Products.showError(response.responseJSON.errors);
            });
    };

    modules.getDataUpdate = function (element) {
        let url = element.data('action');
        Base.callApiNormally(url)
            .then(function (response) {
                Products.fillData(response.data);
            });
    };

    modules.fillData = function (data) {
        for (const field in data) {
            $(`#${field}_edit`).val(data[field]);
        }
        $('#form-edit-product').attr('data-action', data.route);
        $('.image_edit').attr('src', data.image);
        // Pluck categories id
        let category_ids = data.categories.data.map(categories => categories.category_id);
        $('.form-check-edit').each(function (key, value) {
            if (category_ids.includes(parseInt(value.id))) {
                $(this).prop('checked', true);
            } else {
                $(this).prop('checked', false);
            }
        });
    };

    modules.updateProduct = function () {
        let url = $('#form-edit-product').attr('data-action');
        let data = new FormData(document.getElementById('form-edit-product'));
        data.append('_method', 'PUT');
        Base.callApiWithFormData(url, data, "POST")
            .then(function (result) {
                CustomAlert.alertSucces(result.message);
                Products.getList();
                $('#modal-edit-product').modal('hide');
            })
            .catch(function (response) {
                Products.showError(response.responseJSON.errors);
            });

    };

    modules.deleteProduct = function (element) {
        let url = element.data('action');
        CustomAlert.confirmButton("Are you sure you want to delete this product")
            .then(function (result) {
                if (result) {
                    Base.callApiNormally(url, {}, 'DELETE')
                        .then(function (result) {
                            CustomAlert.alertSucces(result.message);
                            Products.getList();
                        })
                        .catch(function (response) {
                            CustomAlert.alertError(response.message);
                        });
                }
            });
    };

    modules.showProduct = function (element) {
        let url = element.data('action');
        Base.callApiNormally(url, {}, 'GET')
            .then(function (response) {
                Products.fillData(response.data);
                Products.changeStatusForm(true);
            });
    };

    modules.changeStatusForm = function (status) {
        $('.form-edit').attr('disabled', status);
        if (status) {
            $('.close-image').hide();
            $('.btn-update-product').hide();
            $('.modal-title').hide();
            $('.image-input').hide();
        } else {
            $('.close-image').show();
            $('.btn-update-product').show();
            $('.modal-title').show();
            $('.image-input').show();
        }
    };

    modules.focusInputError = function (name) {
        $(`#${name}`).focus();
    };

    modules.deleteCategory = function () {
        $('.btn.btn-danger').on('click', function (event) {
            event.preventDefault();
            CustomAlert.confirmButton('Are you sure you want to delete', 'Delete successfully', 'Delete cancelled')
                .then(function (result) {
                });
        });
    };

    modules.closeImage = function () {
        $('.btn-close.close-image').click();
    };

    return modules;
})();

$(document).ready(function () {
    Products.getList($('#list').data('action'));
    $(document).on('click', '.page-link', function (e) {
        e.preventDefault();
        Products.getListByPageLink($(this));
    });

    $(document).on('click', '.btn-store-product', function (e) {
        e.preventDefault();
        Products.storeProduct();
    });

    $(document).on('keyup', '.search-field', debounce(function (e) {
        e.preventDefault();
        Products.getList();
    }));

    $(document).on('change', '.select2', debounce(function (e) {
        e.preventDefault();
        Products.getList();
    }));

    $(document).on('click', '.btn-create', function (e) {
        e.preventDefault();
        Products.showError();
        Products.resetForm();
    });

    $(document).on('click', '.btn-edit', function (e) {
        e.preventDefault();
        Products.showError();
        Products.getDataUpdate($(this));
    });

    $(document).on('click', '.btn-update-product', function (e) {
        e.preventDefault();
        Products.updateProduct();
    });

    $(document).on('click', '.btn-delete', function (e) {
        e.preventDefault();
        Products.deleteProduct($(this));
    });

    $(document).on('click', '.btn-show', function (e) {
        e.preventDefault();
        Products.showProduct($(this));
    });

    Products.deleteCategory();
});



