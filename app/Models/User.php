<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

//    protected $with = ['roles.permissions'];
    const STORAGE_PATH = '/uploads/';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'birthday',
        'address',
        'phone_number',
        'gender',
        'image'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function hasRole($role)
    {
        return $this->roles->contains('name', $role);
    }

    public function hasPermission($permission)
    {
        foreach ($this->roles as $role) {
            if ($role->hasPermission($permission)) {
                return true;
            }
        }
        return false;
    }

    public function detachRoles(array $roleId = [])
    {
        return $this->roles()->detach($roleId);
    }

    public function syncRoles( array $roleId)
    {
        return $this->roles()->sync($roleId);
    }

    public function isSuperAdmin()
    {
        return $this->hasRole('super-admin');
    }

    public function scopeWithName($query, $name)
    {
        return $query->where('name', 'like', '%' . $name . '%');

    }

    public function scopeWithEmail($query, $email)
    {
        return $email ? $query->where('email', $email) : null;

    }

    public function scopeWithRoleId($query, $roleId)
    {
        return $roleId ? $query->whereHas('roles', function ($query) use ($roleId) {
            $query->where('role_id', $roleId);
        }) : null;

    }

    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => asset( self::STORAGE_PATH . $value)
        );

    }

    protected function gender(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $value ? 'Female' : 'Male'
        );
    }

}
