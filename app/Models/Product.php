<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use League\CommonMark\Extension\Attributes\Node\Attributes;

class Product extends Model
{
    use HasFactory;

    const STORAGE_PATH = '/uploads/';

    protected $fillable = [
        'name',
        'price',
        'description',
        'image'
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function syncCategory($categoryId)
    {
        return $this->categories()->sync($categoryId);
    }

    public function detachCategory(array $categoryId = [])
    {
        return $this->categories()->detach($categoryId);
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', '%' . $name . '%') : null;
    }

    public function scopeWithCategory($query, $categoryId)
    {
//        return $categoryId ? $query->whereHas( 'categories', function ($query) use ($categoryId) {
//            return $query->where('category_id', $categoryId);
//        }) : null;
        return $categoryId ? $query->whereHas('categories', fn($category) => $category->where('category_id', $categoryId)) : null;
    }


    public function scopeWithMinPrice($query, $minPrice)
    {
        return $minPrice ? $query->where('price', '>', $minPrice) : null;
    }

    public function scopeWithMaxPrice($query, $maxPrice)
    {
        return $maxPrice ? $query->where('price', '<', $maxPrice) : null;
    }

    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => asset( self::STORAGE_PATH . $value)
        );

    }
}
