<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
//    protected $with = ['permissions'];
    const STORAGE_PATH = '/uploads/';

    protected $fillable = [
        'name',
        'display_name',
        'image'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    public function scopeWithName($query, $name)
    {
        return $query->where('display_name', 'like', '%' . $name . '%');
    }

    public function hasPermission($permission)
    {
        return $this->permissions->contains('name', $permission);
    }

    public function syncPermissions($permissionId)
    {
        return $this->permissions()->sync($permissionId);
    }

    public function detachPermissions(array $permissionId = [])
    {
        return $this->permissions()->detach($permissionId);
    }

    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn($value) => asset(self::STORAGE_PATH . $value)
        );

    }

}
