<?php

namespace App\Services;

use App\Repositories\Permission\PermissionRepositoryInterface;
use App\Repositories\Category\CategoryRepositoryInterface;
use App\Traits\HandleImage;

class CategoryService
{

    protected $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function all()
    {
        return $this->categoryRepository->all();
    }

    public function search($request)
    {
        $dataSearch['name'] = $request->name ?? '';
        $dataSearch['parent_id'] = $request->parent_id ?? '';
        $dataQuantity['paginate_number'] = 10;
        return $this->categoryRepository->search($dataSearch, $dataQuantity);
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $category = $this->categoryRepository->create($dataCreate);
        return $category;
    }

    public function findOrFail(int $id)
    {
        return $this->categoryRepository->findOrFail($id);
    }

    public function update($request, int $id)
    {
        $category = $this->categoryRepository->findOrFail($id);
        $dataUpdate = $request->all();
        $category->update($dataUpdate);
        return $category;
    }

    public function delete(int $id)
    {
        $category = $this->categoryRepository->findOrFail($id);
        $category->childrens()->each(function ($child) {
            $child->parent_id = null;
            $child->save();
        });
        $category->delete();
        return $category;
    }

    public function getParent()
    {
        return $this->categoryRepository->getParent();
    }

    public function getCategoriesByParent(int $id)
    {
        return $this->categoryRepository->find($id)->childrens()->paginate(5);
    }
}
