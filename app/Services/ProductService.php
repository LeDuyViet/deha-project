<?php

namespace App\Services;

use App\Repositories\Product\ProductRepositoryInterface;
use App\Traits\HandleImage;

class ProductService
{
    use HandleImage;

    protected $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function all()
    {
        return $this->productRepository->all();
    }

    public function search($request)
    {
        $dataSearch['name'] = $request->name ?? '';
        $dataSearch['category'] = $request->category ?? '';
        $dataSearch['min_price'] = $request->min_price ?? '';
        $dataSearch['max_price'] = $request->max_price ?? '';
        $dataQuantity['paginate_number'] = 8;
        return $this->productRepository->search($dataSearch, $dataQuantity);
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $dataCreate['image'] = $this->savaImage($request);
        $dataCreate['category_ids'] = $dataCreate['category_ids'] ?? [];
        $product = $this->productRepository->create($dataCreate);
        $product->syncCategory($dataCreate['category_ids']);
        return $product;
    }

    public function findOrFail(int $id)
    {
        return $this->productRepository->findOrFail($id);
    }

    public function update($request, int $id)
    {
        $product = $this->productRepository->findOrFail($id);
        $dataUpdate = $request->all();
        $dataUpdate['image'] = $this->updateImage($request, $product->getRawOriginal('image'));
        $dataUpdate['category_ids'] = $request->category_ids ?? [];
        $product->update($dataUpdate);
        $product->syncCategory($dataUpdate['category_ids']);
        return $product;
    }

    public function delete(int $id)
    {
        $product = $this->productRepository->findOrFail($id);
        $product->detachCategory();
        $product->delete();
        return $product;
    }
}
