<?php

namespace App\Services;

use App\Repositories\Permission\PermissionRepositoryInterface;

class PermissionService
{
    protected $permissionRepository;

    public function __construct(PermissionRepositoryInterface $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    public function count()

    {
        return $this->permissionRepository->count()
;
    }

//    public function getAllPermissions()
//    {
//        return $this->permissionRepository->all();
//    }

    public function groupBy(array $column = ['group_name'])
    {
        return $this->permissionRepository->groupBy($column);
    }
}
