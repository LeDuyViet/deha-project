<?php

namespace App\Services;

use App\Repositories\Role\RoleRepositoryInterface;
use App\Traits\HandleImage;

/**
 *
 */
class RoleService
{
    use HandleImage;

    protected $roleRepository;

    public function __construct(RoleRepositoryInterface $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function all()
    {
        return $this->roleRepository->all();
    }

    public function search($request)
    {
        $dataSearch['name'] = $request->name ?? '';
        $dataQuantity['paginate_number'] = $request->paginate_number ?? 8;
        return $this->roleRepository->search($dataSearch, $dataQuantity);
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $dataCreate['image'] = $this->savaImage($request);
        $dataCreate['permission_names'] = $dataCreate['permission_names'] ?? [];
        $role = $this->roleRepository->create($dataCreate);
        $role->permissions()->attach($dataCreate['permission_names']);
        return $role;
    }

    public function findOrFail(int $id)
    {
        return $this->roleRepository->findOrFail($id);
    }

    public function update($request, int $id)
    {
        $role = $this->roleRepository->findOrFail($id);
        $dataUpdate = $request->all();
        $dataUpdate['permission_names'] = $request->permission_names ?? [];
        $dataUpdate['image'] = $this->updateImage($request, $role->getRawOriginal('image'));
        $role->update($dataUpdate);
        $role->syncPermissions($dataUpdate['permission_names']);
        return $role;
    }

    public function delete(int $id)
    {
        $role = $this->roleRepository->findOrFail($id);
        $role->detachPermissions();
        $role->delete();
        return $role;
    }
}
