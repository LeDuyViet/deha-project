<?php

namespace App\Services;

use App\Repositories\User\UserRepositoryInterface;
use App\Traits\HandleImage;
use Carbon\Carbon;
use function PHPUnit\Framework\isEmpty;

class UserService
{
    use HandleImage;

    protected $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function all()
    {
        return $this->userRepository->all();
    }

    public function search($request)
    {
        $dataSearch['name'] = $request->name ?? '';
        $dataSearch['email'] = $request->email ?? '';
        $dataSearch['role_id'] = $request->role_id ?? '';
        $dataQuantity['paginate_number'] = $request->paginate_number ?? 10;
        return $this->userRepository->search($dataSearch, $dataQuantity);
    }

    public function create($request)
    {
        $dataCreate = $request->all();
        $dataCreate['role_ids'] = $request->role_ids ?? [];
        $dataCreate['image'] = $this->savaImage($request);
        $dataCreate['birthday'] = Carbon::parse($dataCreate['birthday'])->format('Y-m-d H:i:s');
        $user = $this->userRepository->create($dataCreate);
        $user->roles()->attach($dataCreate['role_ids']);
        return $user;
    }

    public function findOrFail(int $id)
    {
        return $this->userRepository->findOrFail($id);
    }

    public function update($request, int $id)
    {
        $user = $this->userRepository->findOrFail($id);
        $dataUpdate = $request->all();
        $dataUpdate['password'] = !empty($dataUpdate['password']) ? bcrypt($dataUpdate['password']) : $user->password;
        $dataUpdate['role_ids'] = $request->role_ids ?? [];
        $dataUpdate['image'] = $this->updateImage($request, $user->getRawOriginal('image'));
        $user->update($dataUpdate);
        $user->syncRoles($dataUpdate['role_ids']);
        return $user;

    }

    public function delete(int $id)
    {
        $user = $this->userRepository->findOrFail($id);
        $user->detachRoles();
        $user->delete();
        return $user;
    }
}
