<?php

namespace App\Http\Requests\User;

use App\Rules\VietNamesePhoneRule;
use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:users,name',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:8',
            'phone_number' => ['required', new VietNamesePhoneRule],
            'image' => 'max:10240|mimes:png,jpg,jpeg,gif,svg,webp',
        ];
    }
}
