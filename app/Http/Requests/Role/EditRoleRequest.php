<?php

namespace App\Http\Requests\Role;

use Illuminate\Foundation\Http\FormRequest;

class EditRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
 * Get the validation rules that apply to the request.rol
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required','unique:roles,name,'.$this->role->id],
            'display_name' => ['required', 'unique:roles,display_name,'.$this->role->id],
            'image' => ['max:10240|mimes:png,jpg,jpeg,gif,svg,webp',]
        ];
    }
}
