<?php

namespace App\Http\Requests\Product;

use App\Traits\HandleErrorValidateJson;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class EditProductRequest extends FormRequest
{
    use HandleErrorValidateJson;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'price' => 'required|numeric',
            'description' => 'required',
            'image' => 'max:10240|mimes:png,jpg,jpeg,gif,svg,webp',
        ];
    }


    protected function failedValidation(Validator $validator)
    {
        $this->failedValidationJson($validator);
    }
}
