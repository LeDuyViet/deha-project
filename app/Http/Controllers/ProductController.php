<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\CreateProductRequest;
use App\Http\Requests\Product\EditProductRequest;
use App\Http\Resources\ProductResource;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function index()
    {
        return view('products.index');
    }

    public function list(Request $request)
    {
        $products = $this->productService->search($request);
        return view('products.list', [
            'products' => $products,
        ])->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateProductRequest $request)
    {
        $product = $this->productService->create($request);
        $productResource = new ProductResource($product);
        return $this->sentSuccesResponse($productResource, 'Product created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->productService->findOrFail($id);
        $productResource = new ProductResource($product);
        return $this->sentSuccesResponse($productResource);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return string
     */
    public function edit($id)
    {
        $product = $this->productService->findOrFail($id);
        $productResource = new ProductResource($product);
        return $this->sentSuccesResponse($productResource);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(EditProductRequest $request, $id)
    {
        $product = $this->productService->update($request, $id);
        $productResource = new ProductResource($product);
        return $this->sentSuccesResponse($productResource, 'Update Product Successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = $this->productService->delete($id);
        $productResource = new ProductResource($product);
        return $this->sentSuccesResponse($productResource, 'Delete Product Successfuly');
    }
}
