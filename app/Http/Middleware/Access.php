<?php

namespace App\Http\Middleware;

use App\Services\AccessService;
use Closure;
use Illuminate\Support\Facades\Auth;

class Access
{
    public function handle($request, Closure $next)
    {
        $accessService = resolve(AccessService::class);

        # current user
        $user = Auth::user();

        if($user) {
            $accessService->setUser($user);
        }

        return $next($request);
    }
}
