<?php

namespace App\Providers;

use App\View\Compose\CategoryCompose;
use App\View\Compose\PermissionCompose;
use App\View\Compose\ProductCompose;
use App\View\Compose\RoleCompose;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['roles.*'], PermissionCompose::class);
        View::composer(['users.*'], RoleCompose::class);
        View::composer(['categories.*'], CategoryCompose::class);
        View::composer(['products.*'], ProductCompose::class);
    }
}
