<?php

namespace App\Providers;

use App\Repositories\Category\CategoryRepository;
use App\Repositories\Category\CategoryRepositoryInterface;
use App\Repositories\Permission\PermissionRepository;
use App\Repositories\Permission\PermissionRepositoryInterface;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Product\ProductRepositoryInterface;
use App\Repositories\Role\RoleRepository;
use App\Repositories\Role\RoleRepositoryInterface;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryInterface;
use App\Services\PermissionService;
use App\Services\ProductService;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{

    public $singletons = [
        PermissionRepositoryInterface::class => PermissionRepository::class,
        RoleRepositoryInterface::class => RoleRepository::class,
        UserRepositoryInterface::class => UserRepository::class,
        CategoryRepositoryInterface::class => CategoryRepository::class,
        ProductRepositoryInterface::class => ProductRepository::class
    ];
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
//
//        $service = new ProductService(new ProductRepository());
//        $this->app->instance(ProductService::class, $service);

//        $this->app->singleton(ProductService::class, ProductService::class);

//        $this->app->bind(ProductRepositoryInterface::class, ProductRepository::class);

//        $this->app->resolving( function ($permission, $app) {
//            dump($permission);
//        });

    }
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
