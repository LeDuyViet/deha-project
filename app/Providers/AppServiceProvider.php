<?php

namespace App\Providers;


use App\Services\AccessService;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//        config(['app.timezone' => 'Asia/Ho_Chi_Minh']);
//        dd(config('test.name'));
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
        Blade::if('hasPermission', function ($permission) {
            return auth()->user()->loadMissing('roles.permissions')->hasPermission($permission)|| auth()->user()->isSuperAdmin();
        });

        Blade::if('hasRole', function ($role) {
            return auth()->user()->hasRole($role) || auth()->user()->isSuperAdmin();
        });

        Blade::if('hideInMyRole', function ($role) {
            return  $role == 'super-admin' ? false : !auth()->user()->hasRole($role);
        });

        Blade::if('hideMyAccount', function ($userId) {
            return  ($userId != auth()->user()->id);
        });
    }
}
