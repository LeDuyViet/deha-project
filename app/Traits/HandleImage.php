<?php


namespace App\Traits;

use Intervention\Image\Facades\Image;

trait HandleImage
{
    protected $imageDefault = 'default.jpg';
    protected $path = 'uploads/';

    public function verifyImage($request)
    {
        return $request->hasFile('image');
    }

    public function savaImage($request)
    {
        $image = $this->imageDefault;
        if($this->verifyImage($request))
        {
            $file = $request->file('image');
            $filename = time() . $file->getClientOriginalName();
            $saveLocation = $this->path . $filename;
            $image = Image::make($file);
            $image->fit(150, 150)->save($saveLocation);
            return $filename;
        }
        return $image;
    }

    public function deleteImage($imageName)
    {
        $pathName = $this->path . $imageName;
        if(!empty($imageName) && file_exists($pathName) && $imageName != $this->imageDefault)
        {
            unlink($pathName);
        }
    }
    public function updateImage($request, $currentImage)
    {
//        dd($this->verifyImage($request), $currentImage);
        if($this->verifyImage($request))
        {
            $this->deleteImage($currentImage);
            return $this->savaImage($request);
        } else {
            return $currentImage;
        }
    }
}
