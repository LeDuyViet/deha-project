<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface RepositoryInterface
{
    /**
     * Retrieve all data of repository
     * @return Collection|Model[] `
     */
    public function all();

    /**
     * Retrieve all data of repository, paginated
     * @param null $limit
     * @param array $columns
     * @return
     */
    public function paginate($limit = null, $columns = ['*']);

    /**
     * Find data by id
     * @param $id
     * @param array $columns
     * @return
     */
    public function find($id);

    public function findWithoutRedirect($id, $columns = ['*']);

    public function findOrFail($id, $columns = ['*']);

    public function findOrFailWithTrashed($id, $columns = ['*']);

    /**
     * Save a new entity in repository
     * @param array $input
     * @return
     */
    public function create(array $input);

    /**
     * Update a entity in repository by id
     * @param array $input
     * @param $id
     * @return BaseRepository
     */
    public function update(array $input, $id);

    /**
     * Delete a entity in repository by id
     *
     * @param $id
     *
     * @return int
     */
    public function delete($id);

    public function multipleDelete(array $ids);

    public function latest($id);

    public function updateOrCreate(array $arrayFind, $arrayCreate = ['*']);

    public function insertMany($data);

    public function count();

}
