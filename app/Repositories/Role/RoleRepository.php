<?php

namespace App\Repositories\Role;

use App\Models\Role;
use App\Repositories\BaseRepository;

class RoleRepository extends BaseRepository implements RoleRepositoryInterface
{
    public function model()
    {
        return Role::class;
    }

    public function search(array $dataSearch, $dataQuantity)
    {
        return $this->model->withCount(['permissions', 'users'])->withName($dataSearch['name'])->latest()->paginate($dataQuantity['paginate_number']);
    }
}
