<?php

namespace App\Repositories\Product;

use App\Models\Product;
use App\Repositories\BaseRepository;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{

    public function model()
    {
        return Product::class;
    }

    /**
     * @inheritDoc
     */
    public function search(array $dataSearch, $dataQuantity)
    {
        return $this->model
            ->withName($dataSearch['name'])
            ->withCategory($dataSearch['category'])
            ->withMinPrice($dataSearch['min_price'])
            ->withMaxPrice($dataSearch['max_price'])
            ->latest()
            ->paginate($dataQuantity['paginate_number']);
    }
}
