<?php

namespace App\Repositories\Product;

use App\Repositories\RepositoryInterface;

interface ProductRepositoryInterface extends RepositoryInterface
{
    /**
     * @param array $dataSearch
     * @param $dataQuantity
     * @return mixed
     */
    public function search(array $dataSearch, $dataQuantity);
}
