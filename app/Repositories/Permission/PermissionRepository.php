<?php

namespace App\Repositories\Permission;

use App\Models\Permission;
use App\Repositories\BaseRepository;

class PermissionRepository extends BaseRepository implements PermissionRepositoryInterface
{
    public function model()
    {
        return Permission::class;
    }

    public function search(string $dataSearch)
    {
        return $this->model->withName($dataSearch)->latest()->paginate(8);
    }

    public function count()
    {
        return $this->model->count();
    }

    public function groupBy(array $column = ['group_name'])
    {
        return $this->model->all()->groupBy($column);
    }
}
