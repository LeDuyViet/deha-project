<?php

namespace App\Repositories\Permission;

use App\Repositories\RepositoryInterface;

interface PermissionRepositoryInterface extends RepositoryInterface
{
    public function search(string $dataSearch);

    public function count();

    public function groupBy(array $column);
}
