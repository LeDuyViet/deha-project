<?php

namespace App\Repositories\Category;

use App\Repositories\RepositoryInterface;

interface CategoryRepositoryInterface extends RepositoryInterface
{
    public function search(array $dataSearch, $dataQuantity);

    public function getParent();

}
