<?php

namespace App\Repositories\Category;

use App\Models\Category;
use App\Repositories\BaseRepository;

class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface
{
    public function model()
    {
        return Category::class;
    }

    public function search(array $dataSearch, $dataQuantity)
    {
        return $this->model
            ->withName($dataSearch['name'])
            ->withParentCategory($dataSearch['parent_id'])
            ->latest()
            ->paginate($dataQuantity['paginate_number']);
    }

    public function getParent()
    {
        return $this->model->whereNull('parent_id')->get();
    }
}
