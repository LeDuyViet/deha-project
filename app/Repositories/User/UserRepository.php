<?php

namespace App\Repositories\User;

use App\Models\User;
use App\Repositories\BaseRepository;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function model()
    {
        return User::class;
    }

    public function search(array $dataSearch, $dataQuantity)
    {
        return $this->model->withName($dataSearch['name'])->withEmail($dataSearch['email'])->withRoleId($dataSearch['role_id'])->latest()->paginate($dataQuantity['paginate_number']);
    }
}
