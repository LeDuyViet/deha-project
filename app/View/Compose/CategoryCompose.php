<?php

namespace App\View\Compose;

use App\Services\CategoryService;
use Illuminate\View\View;

class CategoryCompose
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function compose(View $view)
    {
        $view->with('parentCategories', $this->categoryService->getParent());
//        $view->with('categories', $this->categoryService->all());
    }
}
