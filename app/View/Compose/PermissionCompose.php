<?php

namespace App\View\Compose;


use App\Services\PermissionService;
use Illuminate\View\View;

class PermissionCompose
{
    protected $permissionService;

    public function __construct(PermissionService $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    public function compose(View $view)
    {
        $view->with('permissionGroups', $this->permissionService->groupBy(['group_name']));
        $view->with('permissionCount', $this->permissionService->count()
);
    }

}
