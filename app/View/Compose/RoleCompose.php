<?php

namespace App\View\Compose;


use App\Services\RoleService;
use Illuminate\View\View;

class RoleCompose
{
    protected $roleService;

    public function __construct(RoleService $roleService)
    {
        $this->roleService = $roleService;
    }

    public function compose(View $view)
    {
        $view->with('roles', $this->roleService->all());
    }

}
