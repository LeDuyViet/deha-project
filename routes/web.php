<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::prefix('/roles')
    ->name('roles.')
    ->middleware(['auth'])
    ->group(function() {
    Route::controller(RoleController::class)
        ->group(function() {
        Route::get('/', 'index')
            ->name('index')
            ->middleware('check.permission:role-view');
        Route::get('/show/{id}', 'show')
            ->name('show')
            ->middleware('check.permission:role-show');
        Route::get('/create', 'create')
            ->name('create')
            ->middleware('check.permission:role-create');
        Route::post('/create', 'store')
            ->name('store')
            ->middleware('check.permission:role-store');
        Route::get('/edit/{id}', 'edit')
            ->name('edit')
            ->middleware('check.permission:role-edit');
        Route::put('/{role}', 'update')
            ->name('update')
            ->middleware('check.permission:role-update');
        Route::delete('/{id}', 'destroy')
            ->name('delete')
            ->middleware('check.permission:role-delete');
    });
});

Route::prefix('/users')
    ->name('users.')
    ->middleware('auth')
    ->group(function() {
    Route::controller(UserController::class)
        ->group(function() {
        Route::get('/', 'index')
            ->name('index')
            ->middleware('check.permission:user-view');
        Route::get('/show/{id}', 'show')
            ->name('show')
            ->middleware('check.permission:user-show');
        Route::get('/create', 'create')
            ->name('create')
            ->middleware('check.permission:user-create');
        Route::post('/create', 'store')
            ->name('store')
            ->middleware('check.permission:user-store');
        Route::get('/edit/{id}', 'edit')
            ->name('edit')
            ->middleware('check.permission:user-edit');
        Route::put('/{id}', 'update')
            ->name('update')
            ->middleware('check.permission:user-update');
        Route::delete('/{id}', 'destroy')
            ->name('delete')
            ->middleware('check.permission:user-delete');
    });
});

Route::prefix('/categories')
    ->name('categories.')
    ->middleware('auth')
    ->group(function() {
    Route::controller(CategoryController::class)
        ->group(function() {
        Route::get('/', 'index')
            ->name('index')
            ->middleware('check.permission:category-view');
        Route::get('/show/{id}', 'show')
            ->name('show')
            ->middleware('check.permission:category-show');
        Route::get('/create', 'create')
            ->name('create')
            ->middleware('check.permission:category-create');
        Route::post('/create', 'store')
            ->name('store')
            ->middleware('check.permission:category-store');
        Route::get('/edit/{id}', 'edit')
            ->name('edit')
            ->middleware('check.permission:category-edit');
        Route::put('/{id}', 'update')
            ->name('update')
            ->middleware('check.permission:category-update');
        Route::delete('/{id}', 'destroy')
            ->name('delete')
            ->middleware('check.permission:category-delete');
    });
});


Route::prefix('/products')
    ->name('products.')
    ->middleware('auth')
    ->group(function() {
    Route::controller(ProductController::class)
        ->group(function() {
        Route::get('/', 'index')
            ->name('index')
            ->middleware('check.permission:product-view');
        Route::get('/list', 'list')
            ->name('list')
            ->middleware('check.permission:product-view');
        Route::get('/show/{id}', 'show')
            ->name('show')
            ->middleware('check.permission:product-show');
        Route::get('/create', 'create')
            ->name('create')
            ->middleware('check.permission:product-create');
        Route::post('/create', 'store')
            ->name('store')
            ->middleware('check.permission:product-store');
        Route::get('/edit/{id}', 'edit')
            ->name('edit')
            ->middleware('check.permission:product-edit');
        Route::put('/{id}', 'update')
            ->name('update')
            ->middleware('check.permission:product-update');
        Route::delete('/{id}', 'destroy')
            ->name('delete')
            ->middleware('check.permission:product-delete');
    });
});


Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'index'])
    ->name('dashboard')

    ->middleware('auth');
Route::redirect('/', '/dashboard');
